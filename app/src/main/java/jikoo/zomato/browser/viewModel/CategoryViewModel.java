package jikoo.zomato.browser.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Category;
import jikoo.zomato.browser.model.dto.Cuisine;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.repo.CategoryRepository;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CategoryViewModel extends AndroidViewModel{

    CategoryRepository mCategoryRepository ;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
        mCategoryRepository = CategoryRepository.getInstance();
    }

    public MutableLiveData<ArrayList<Category>> getLiveData(){
        return mCategoryRepository.getLiveData();
    }

    public MutableLiveData<ApiException> getErrorLiveData(){
        return mCategoryRepository.getErrorLiveData();
    }

    public void fetch(){
        mCategoryRepository.fetch(this.getApplication().getApplicationContext());
    }
    

}
