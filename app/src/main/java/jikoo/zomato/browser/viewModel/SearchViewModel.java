package jikoo.zomato.browser.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import jikoo.zomato.browser.model.dto.response.SearchResponse;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.repo.SearchRepository;

/**
 * Created by jyotishman.baruah on 09/07/18.
 */

public class SearchViewModel extends AndroidViewModel {

    private SearchRepository mSearchRepository;


    public SearchViewModel(@NonNull Application application) {
        super(application);
        mSearchRepository = SearchRepository.getInstance();
    }

    public MutableLiveData<SearchResponse> getSearchLiveData() {
        return mSearchRepository.getSearchLiveData();
    }

    public MutableLiveData<ApiException> getExceptionLiveData() {
        return mSearchRepository.getExceptionLiveData();
    }

    public void search(String query, double lat, double lon, String categories, String cuisines, String collections, String establishments, int start) {
        mSearchRepository.search(this.getApplication().getApplicationContext(),query, lat, lon, categories, cuisines, collections, establishments, start);
    }

}
