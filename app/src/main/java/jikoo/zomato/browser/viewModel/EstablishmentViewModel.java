package jikoo.zomato.browser.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Establishment;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.repo.EstablishmentRepository;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class EstablishmentViewModel extends AndroidViewModel {

    EstablishmentRepository mEstablishmentRepository ;

    public EstablishmentViewModel(@NonNull Application application) {
        super(application);
        mEstablishmentRepository = EstablishmentRepository.getInstance();
    }

    public MutableLiveData<ArrayList<Establishment>> getLiveData(){
        return mEstablishmentRepository.getLiveData();
    }

    public MutableLiveData<ApiException> getErrorLiveData(){
        return mEstablishmentRepository.getErrorLiveData();
    }

    public void fetch(double lat, double lon){
        mEstablishmentRepository.fetch(this.getApplication().getApplicationContext(),lat,lon);
    }

}
