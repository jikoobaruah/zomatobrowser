package jikoo.zomato.browser.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Category;
import jikoo.zomato.browser.model.dto.Collection;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.repo.CollectionRepository;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CollectionViewModel extends AndroidViewModel {

    CollectionRepository mCollectionRepository ;

    public CollectionViewModel(@NonNull Application application) {
        super(application);
        mCollectionRepository = CollectionRepository.getInstance();
    }

    public MutableLiveData<ArrayList<Collection>> getLiveData(){
        return mCollectionRepository.getLiveData();
    }

    public MutableLiveData<ApiException> getErrorLiveData(){
        return mCollectionRepository.getErrorLiveData();
    }

    public void fetch(double lat, double lon){
        mCollectionRepository.fetch(this.getApplication().getApplicationContext(),lat,lon);
    }

}
