package jikoo.zomato.browser.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.repo.LocationSearchRepository;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class LocationSearchViewModel extends AndroidViewModel {


    private LocationSearchRepository mLocationSearchRepository;

    public LocationSearchViewModel(@NonNull Application application) {
        super(application);
        mLocationSearchRepository = LocationSearchRepository.getInstance();
    }


    public MutableLiveData<ArrayList<Restaurant>> getSearchLiveData() {
        return mLocationSearchRepository.getSearchLiveData();
    }

    public MutableLiveData<ApiException> getExceptionLiveData()  {
        return mLocationSearchRepository.getExceptionLiveData();
    }

    public void searchByLatLon(CharSequence address, double lat, double lon) {
        mLocationSearchRepository.search(this.getApplication().getApplicationContext(),lat, lon);
    }

}
