package jikoo.zomato.browser.viewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Cuisine;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.repo.CuisineRepository;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CuisineViewModel extends AndroidViewModel{

    CuisineRepository mCuisineRepository ;

    public CuisineViewModel(@NonNull Application application) {
        super(application);
        mCuisineRepository = CuisineRepository.getInstance();
    }

    public MutableLiveData<ArrayList<Cuisine>> getLiveData(){
        return mCuisineRepository.getLiveData();
    }

    public MutableLiveData<ApiException> getErrorLiveData(){
        return mCuisineRepository.getErrorLiveData();
    }


    public void fetch(double lat, double lon){
        mCuisineRepository.fetch(this.getApplication().getApplicationContext(),lat,lon);
    }
}
