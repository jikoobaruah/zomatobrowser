package jikoo.zomato.browser.model.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import jikoo.zomato.browser.model.dto.Cuisine;
import jikoo.zomato.browser.model.dto.response.CuisinesResponse;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.network.ZomatoService;
import jikoo.zomato.browser.utils.SingleLiveEvent;
import retrofit2.Response;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CuisineRepository {

    private static CuisineRepository sInstance;

    public static CuisineRepository getInstance(){
        if (sInstance == null)
            sInstance = new CuisineRepository();
        return sInstance;
    }

    private CuisineRepository(){
        mLiveEvent = new SingleLiveEvent<>();
        mErrorLiveEvent = new SingleLiveEvent<>();

    }

    private static Executor sExecutor = Executors.newSingleThreadExecutor();

    private SingleLiveEvent<ArrayList<Cuisine>> mLiveEvent;

    private SingleLiveEvent<ApiException> mErrorLiveEvent;


    public MutableLiveData<ArrayList<Cuisine>> getLiveData() {
        return mLiveEvent;
    }

    public MutableLiveData<ApiException> getErrorLiveData() {
        return mErrorLiveEvent;
    }

    public void fetch(final Context context, final double lat, final double lon) {
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    Response<CuisinesResponse> response = ZomatoService.ZomatoApi.getInstance(context.getApplicationContext()).getZomatoService().getCuisines(lat,lon).execute();

                    if (response.isSuccessful()){
                        mLiveEvent.postValue(response.body().getCuisines());
                    }else {
                        mErrorLiveEvent.postValue(new ApiException(response.errorBody().string()));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    mErrorLiveEvent.postValue(new ApiException(new ApiException.ApiError(400,e.getMessage(),e.getMessage())));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
