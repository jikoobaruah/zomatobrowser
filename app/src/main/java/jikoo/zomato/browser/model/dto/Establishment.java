package jikoo.zomato.browser.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class Establishment implements Parcelable {

    public Establishment(){}

    public Establishment(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Establishment){
            return ((Establishment)obj).id.equals(id);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(id);
    }

    protected Establishment(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<Establishment> CREATOR = new Creator<Establishment>() {
        @Override
        public Establishment createFromParcel(Parcel in) {
            return new Establishment(in);
        }

        @Override
        public Establishment[] newArray(int size) {
            return new Establishment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }
}
