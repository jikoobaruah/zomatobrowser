package jikoo.zomato.browser.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class Cuisine implements Parcelable{

    public Cuisine(){}

    public Cuisine(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @SerializedName("cuisine_id")
    private String id;

    @SerializedName("cuisine_name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Cuisine){
            return ((Cuisine)obj).id.equals(id);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(id);
    }

    protected Cuisine(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<Cuisine> CREATOR = new Creator<Cuisine>() {
        @Override
        public Cuisine createFromParcel(Parcel in) {
            return new Cuisine(in);
        }

        @Override
        public Cuisine[] newArray(int size) {
            return new Cuisine[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }
}
