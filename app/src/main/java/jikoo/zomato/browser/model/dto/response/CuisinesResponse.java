package jikoo.zomato.browser.model.dto.response;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Cuisine;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CuisinesResponse {

    private ArrayList<CuisineWrapper> cuisines ;

    public ArrayList<Cuisine> getCuisines() {
        if (cuisines == null || cuisines.size() ==0 )
            return null;
        ArrayList<Cuisine> cuisinesList = new ArrayList<>();
        for (CuisineWrapper wrapper : cuisines){
            cuisinesList.add(wrapper.getCuisine());
        }
        return cuisinesList;
    }

    public  class CuisineWrapper {

        private Cuisine cuisine;

        public Cuisine getCuisine() {
            return cuisine;
        }
    }
}
