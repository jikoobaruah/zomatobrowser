package jikoo.zomato.browser.model.dto.response;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Collection;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CollectionsResponse {


    private ArrayList<CollectionWrapper> collections ;

    public ArrayList<Collection> getCollections() {
        if (collections == null || collections.size() ==0 )
            return null;

        ArrayList<Collection> collectionsList = new ArrayList<>();
        for (CollectionWrapper wrapper : collections){
            collectionsList.add(wrapper.getCollection());
        }
        return collectionsList;
    }

    private class CollectionWrapper {

        private Collection collection;

        public Collection getCollection() {
            return collection;
        }
    }
}
