package jikoo.zomato.browser.model.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import jikoo.zomato.browser.model.dto.response.GeocodeResponse;
import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.network.ZomatoService;
import jikoo.zomato.browser.utils.SingleLiveEvent;
import retrofit2.Response;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class LocationSearchRepository {


    private static final Executor executor = Executors.newSingleThreadExecutor();
    private static LocationSearchRepository sRepository;

    private SingleLiveEvent<ArrayList<Restaurant>> mSearchLiveData;
    private SingleLiveEvent<ApiException> mExceptionLiveData;

    private LocationSearchRepository() {
        mSearchLiveData = new SingleLiveEvent<>();
        mExceptionLiveData = new SingleLiveEvent<>();
    }

    public static LocationSearchRepository getInstance() {
        if (sRepository == null) {
            sRepository = new LocationSearchRepository();
        }

        return sRepository;
    }

    public MutableLiveData<ArrayList<Restaurant>> getSearchLiveData() {
        return mSearchLiveData;
    }

    public MutableLiveData<ApiException> getExceptionLiveData() {
        return mExceptionLiveData;
    }

    public void search(final Context context, final double lat, final double lon) {

        executor.execute(new Runnable() {
            @Override
            public void run() {

                try {
                    Response<GeocodeResponse> response = ZomatoService.ZomatoApi.getInstance(context.getApplicationContext()).getZomatoService().geocode(lat, lon).execute();
                    if (response.isSuccessful()) {
                        ArrayList<Restaurant> restaurants = new ArrayList<>();
                        for (GeocodeResponse.RestaurantWrapper wrapper : response.body().getRestaurantWrappers()) {
                            restaurants.add(wrapper.getRestaurant());
                        }
                        mSearchLiveData.postValue(restaurants);
                    } else {
                        mExceptionLiveData.postValue(new ApiException(response.errorBody().string()));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    mExceptionLiveData.postValue(new ApiException(new ApiException.ApiError(400,e.getMessage(),e.getMessage())));
                }

            }
        });

    }
}
