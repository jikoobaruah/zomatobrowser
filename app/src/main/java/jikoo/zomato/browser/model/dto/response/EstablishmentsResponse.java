package jikoo.zomato.browser.model.dto.response;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Establishment;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class EstablishmentsResponse {

    private ArrayList<EstablishmentWrapper> establishments ;

    public ArrayList<Establishment> getEstablishments() {
        if (establishments == null || establishments.size() ==0 )
            return null;
        ArrayList<Establishment> establishmentsList = new ArrayList<>();
        for (EstablishmentWrapper wrapper : establishments){
            establishmentsList.add(wrapper.getEstablishment());
        }
        return establishmentsList;
    }

    public class EstablishmentWrapper {
        private Establishment establishment;

        public Establishment getEstablishment() {
            return establishment;
        }
    }
}
