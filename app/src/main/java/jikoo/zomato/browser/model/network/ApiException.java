package jikoo.zomato.browser.model.network;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class ApiException extends RuntimeException {
    private ApiError error;
    public ApiException(String jsonString) {
        error = ApiError.getError(jsonString);

    }

    public ApiException(ApiError apiError) {
        error = apiError;
    }

    public ApiError getError() {
        return error;
    }

    public static class ApiError implements Parcelable{

        private int code;
        private String status;
        private String message;

        public ApiError(){};

        public ApiError(int code, String status, String message) {
            this.code = code;
            this.status = status;
            this.message = message;
        }

        protected ApiError(Parcel in) {
            code = in.readInt();
            status = in.readString();
            message = in.readString();
        }

        public static final Creator<ApiError> CREATOR = new Creator<ApiError>() {
            @Override
            public ApiError createFromParcel(Parcel in) {
                return new ApiError(in);
            }

            @Override
            public ApiError[] newArray(int size) {
                return new ApiError[size];
            }
        };

        public int getCode() {
            return code;
        }

        public String getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public static ApiError getError(String jsonString) {
            try {
                return new Gson().fromJson(jsonString,ApiError.class);
            }catch (Exception ex){
                return new ApiError(400,jsonString,jsonString);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(code);
            dest.writeString(status);
            dest.writeString(message);
        }
    }
}
