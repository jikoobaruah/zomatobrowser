package jikoo.zomato.browser.model.dto.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Restaurant;

/**
 * Created by jyotishman.baruah on 09/07/18.
 */

public class SearchResponse {

    @SerializedName("results_found")
    private int resultsFound;


    @SerializedName("results_start")
    private int resultsStart;

    @SerializedName("results_shown")
    private int resultsShown;

    private ArrayList<RestaurantWrapper> restaurants;

    public int getResultsFound() {
        return resultsFound;
    }

    public int getResultsStart() {
        return resultsStart;
    }

    public int getResultsShown() {
        return resultsShown;
    }

    public ArrayList<RestaurantWrapper> getRestaurants() {
        return restaurants;
    }

    public static class RestaurantWrapper {
        private Restaurant restaurant;

        public Restaurant getRestaurant() {
            return restaurant;
        }
    }
}
