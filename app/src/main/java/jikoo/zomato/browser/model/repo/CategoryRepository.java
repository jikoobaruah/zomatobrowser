package jikoo.zomato.browser.model.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import jikoo.zomato.browser.model.dto.response.CategoriesResponse;
import jikoo.zomato.browser.model.dto.Category;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.network.ZomatoService;
import jikoo.zomato.browser.utils.SingleLiveEvent;
import retrofit2.Response;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CategoryRepository {


    private static CategoryRepository sInstance;

    public static CategoryRepository getInstance() {
        if (sInstance == null)
            sInstance = new CategoryRepository();
        return sInstance;
    }

    private CategoryRepository() {
        mLiveEvent = new SingleLiveEvent<>();
        mErrorLiveEvent = new SingleLiveEvent<>();

    }

    private static Executor sExecutor = Executors.newSingleThreadExecutor();

    private SingleLiveEvent<ArrayList<Category>> mLiveEvent;

    private SingleLiveEvent<ApiException> mErrorLiveEvent;


    public MutableLiveData<ArrayList<Category>> getLiveData() {
        return mLiveEvent;
    }

    public MutableLiveData<ApiException> getErrorLiveData() {
        return mErrorLiveEvent;
    }

    public void fetch(final Context context) {
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    Response<CategoriesResponse> response = ZomatoService.ZomatoApi.getInstance(context.getApplicationContext()).getZomatoService().getCategories().execute();

                    if (response.isSuccessful()){
                        mLiveEvent.postValue(response.body().getCategories());
                    }else {
                        mErrorLiveEvent.postValue(new ApiException(response.errorBody().string()));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    mErrorLiveEvent.postValue(new ApiException(new ApiException.ApiError(400,e.getMessage(),e.getMessage())));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}