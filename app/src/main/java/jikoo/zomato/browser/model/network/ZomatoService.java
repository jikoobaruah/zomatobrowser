package jikoo.zomato.browser.model.network;

import android.content.Context;

import java.io.File;
import java.util.HashMap;

import jikoo.zomato.browser.BuildConfig;
import jikoo.zomato.browser.model.dto.response.CategoriesResponse;
import jikoo.zomato.browser.model.dto.response.CollectionsResponse;
import jikoo.zomato.browser.model.dto.response.CuisinesResponse;
import jikoo.zomato.browser.model.dto.response.EstablishmentsResponse;
import jikoo.zomato.browser.model.dto.response.GeocodeResponse;
import jikoo.zomato.browser.model.dto.response.SearchResponse;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public interface ZomatoService {

    String BASE_URL = "https://developers.zomato.com/api/v2.1/";

    @Headers("user-key:"+ BuildConfig.API_KEY)
    @GET("geocode")
    Call<GeocodeResponse> geocode(@Query("lat") double lat, @Query("lon") double lon);

    @Headers("user-key:"+ BuildConfig.API_KEY)
    @GET("categories")
    Call<CategoriesResponse> getCategories();

    @Headers("user-key:"+ BuildConfig.API_KEY)
    @GET("collections")
    Call<CollectionsResponse> getCollections(@Query("lat") double lat, @Query("lon") double lon);

    @Headers("user-key:"+ BuildConfig.API_KEY)
    @GET("cuisines")
    Call<CuisinesResponse> getCuisines(@Query("lat") double lat, @Query("lon") double lon);

    @Headers("user-key:"+ BuildConfig.API_KEY)
    @GET("establishments")
    Call<EstablishmentsResponse> getEstablishments(@Query("lat") double lat, @Query("lon") double lon);

    @Headers("user-key:"+ BuildConfig.API_KEY)
    @GET("search")
    Call<SearchResponse> search(@QueryMap HashMap<String, String> queryMap);


    class ZomatoApi {

        private ZomatoService mZomatoService;

        private ZomatoApi(Context context){
            File httpCacheDirectory = new File(context.getApplicationContext().getCacheDir(), "responses");
            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            Cache cache = new Cache(httpCacheDirectory, cacheSize);

            OkHttpClient.Builder builder = new OkHttpClient.Builder().cache(cache);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ZomatoService.BASE_URL)
                    .client(builder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mZomatoService = retrofit.create(ZomatoService.class);
        }

        private static ZomatoApi sInstance;

        public static  ZomatoApi getInstance(Context context){
           if (sInstance == null)
               sInstance = new ZomatoApi(context);
           return sInstance;
        }

        public ZomatoService getZomatoService() {
            return mZomatoService;
        }


    }
}
