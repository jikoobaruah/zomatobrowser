package jikoo.zomato.browser.model.dto;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jyotishman.baruah on 04/07/18.
 */

public class Restaurant  implements Parcelable{


    public Restaurant(){

    }

    private String id;

    private String name;

    private String url;

    private Location location;

    private String cuisines;

    @SerializedName("average_cost_for_two")
    private String costForTwo;

    private String currency;

    @SerializedName("featured_image")
    private String image;

    @SerializedName("user_rating")
    private UserRating userRating;


    protected Restaurant(Parcel in) {
        id = in.readString();
        name = in.readString();
        url = in.readString();
        location = in.readParcelable(Location.class.getClassLoader());
        cuisines = in.readString();
        costForTwo = in.readString();
        currency = in.readString();
        image = in.readString();
        userRating = in.readParcelable(UserRating.class.getClassLoader());
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public Location getLocation() {
        return location;
    }

    public String getCuisines() {
        return cuisines;
    }

    public String getCostForTwo() {
        return costForTwo;
    }

    public String getCurrency() {
        return currency;
    }

    public String getImage() {
        return image;
    }

    public String getId() {
        return id;
    }

    public UserRating getUserRating() {
        return userRating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(url);
        dest.writeParcelable(location, flags);
        dest.writeString(cuisines);
        dest.writeString(costForTwo);
        dest.writeString(currency);
        dest.writeString(image);
        dest.writeParcelable(userRating, flags);
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Restaurant) {
            return Integer.parseInt(id) == Integer.parseInt(((Restaurant)obj).id);
        }
        return super.equals(obj);
    }

    public static class Location implements Parcelable{

        public Location(){}

        private String address;
        private String locality;
        private String city;
        private int city_id;
        private String latitude;
        private String longitude;
        private String zipcode;

        @SerializedName("country_id")
        private int countryId;

        @SerializedName("locality_verbose")
        private String localityVerbose;

        protected Location(Parcel in) {
            address = in.readString();
            locality = in.readString();
            city = in.readString();
            city_id = in.readInt();
            latitude = in.readString();
            longitude = in.readString();
            zipcode = in.readString();
            countryId = in.readInt();
            localityVerbose = in.readString();
        }

        public static final Creator<Location> CREATOR = new Creator<Location>() {
            @Override
            public Location createFromParcel(Parcel in) {
                return new Location(in);
            }

            @Override
            public Location[] newArray(int size) {
                return new Location[size];
            }
        };

        public String getAddress() {
            return address;
        }

        public String getLocality() {
            return locality;
        }

        public String getCity() {
            return city;
        }

        public int getCity_id() {
            return city_id;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public String getZipcode() {
            return zipcode;
        }

        public int getCountryId() {
            return countryId;
        }

        public String getLocalityVerbose() {
            return localityVerbose;
        }

        public LatLng getLatLng(){
            return new LatLng(Double.parseDouble(getLatitude()), Double.parseDouble(getLongitude()));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(address);
            dest.writeString(locality);
            dest.writeString(city);
            dest.writeInt(city_id);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(zipcode);
            dest.writeInt(countryId);
            dest.writeString(localityVerbose);
        }
    }

    public static class UserRating implements Parcelable{

        @SerializedName("aggregate_rating")
        private String aggregateRating;

        @SerializedName("rating_text")
        private String ratingText;

        @SerializedName("rating_color")
        private String ratingColor;

        private String votes;

        protected UserRating(Parcel in) {
            aggregateRating = in.readString();
            ratingText = in.readString();
            ratingColor = in.readString();
            votes = in.readString();
        }

        public static final Creator<UserRating> CREATOR = new Creator<UserRating>() {
            @Override
            public UserRating createFromParcel(Parcel in) {
                return new UserRating(in);
            }

            @Override
            public UserRating[] newArray(int size) {
                return new UserRating[size];
            }
        };

        public String getAggregateRating() {
            return aggregateRating;
        }

        public String getRatingText() {
            return ratingText;
        }

        public String getRatingColor() {
            return "#"+ratingColor;
        }

        public String getVotes() {
            return votes;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(aggregateRating);
            dest.writeString(ratingText);
            dest.writeString(ratingColor);
            dest.writeString(votes);
        }
    }
}
