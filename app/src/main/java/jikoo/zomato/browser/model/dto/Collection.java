package jikoo.zomato.browser.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class Collection implements Parcelable{

    public Collection(){}

    public Collection(String id, String title, String url, String description, String imageUrl, String resCount, String shareUrl) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.description = description;
        this.imageUrl = imageUrl;
        this.resCount = resCount;
        this.shareUrl = shareUrl;
    }

    @SerializedName("collection_id")
    private String id;

    private String title;

    private String url;

    private String description;

    private String imageUrl;

    @SerializedName("res_count")
    private String resCount;

    @SerializedName("share_url")
    private String shareUrl;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getResCount() {
        return resCount;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Collection){
            return ((Collection)obj).id.equals(id);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(id);
    }

    protected Collection(Parcel in) {
        id = in.readString();
        title = in.readString();
        url = in.readString();
        description = in.readString();
        imageUrl = in.readString();
        resCount = in.readString();
        shareUrl = in.readString();
    }

    public static final Creator<Collection> CREATOR = new Creator<Collection>() {
        @Override
        public Collection createFromParcel(Parcel in) {
            return new Collection(in);
        }

        @Override
        public Collection[] newArray(int size) {
            return new Collection[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(url);
        dest.writeString(description);
        dest.writeString(imageUrl);
        dest.writeString(resCount);
        dest.writeString(shareUrl);
    }
}
