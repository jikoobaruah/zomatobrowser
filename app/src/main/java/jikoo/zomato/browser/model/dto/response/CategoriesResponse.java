package jikoo.zomato.browser.model.dto.response;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Category;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CategoriesResponse {

    private ArrayList<CategoryWrapper> categories ;

    public ArrayList<Category> getCategories() {
        if (categories == null || categories.size() ==0 )
            return null;
        ArrayList<Category> categoriesList = new ArrayList<>();
        for (CategoryWrapper wrapper : categories){
            categoriesList.add(wrapper.getCategories());
        }
        return categoriesList;
    }

    private class CategoryWrapper {

        private Category categories;

        public Category getCategories() {
            return categories;
        }
    }
}
