package jikoo.zomato.browser.model.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import jikoo.zomato.browser.model.dto.Collection;
import jikoo.zomato.browser.model.dto.response.CollectionsResponse;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.network.ZomatoService;
import jikoo.zomato.browser.utils.SingleLiveEvent;
import retrofit2.Response;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class CollectionRepository {

    private static CollectionRepository sInstance;

    public static CollectionRepository getInstance() {
        if (sInstance == null)
            sInstance = new CollectionRepository();
        return sInstance;
    }

    private CollectionRepository() {
        mLiveEvent = new SingleLiveEvent<>();
        mErrorLiveEvent = new SingleLiveEvent<>();

    }

    private static Executor sExecutor = Executors.newSingleThreadExecutor();

    private SingleLiveEvent<ArrayList<Collection>> mLiveEvent;

    private SingleLiveEvent<ApiException> mErrorLiveEvent;


    public MutableLiveData<ArrayList<Collection>> getLiveData() {
        return mLiveEvent;
    }

    public MutableLiveData<ApiException> getErrorLiveData() {
        return mErrorLiveEvent;
    }

    public void fetch(final Context context, final double lat, final double lon) {
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    Response<CollectionsResponse> response = ZomatoService.ZomatoApi.getInstance(context.getApplicationContext()).getZomatoService().getCollections(lat,lon).execute();

                    if (response.isSuccessful()){
                        mLiveEvent.postValue(response.body().getCollections());
                    }else {
                        mErrorLiveEvent.postValue(new ApiException(response.errorBody().string()));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    mErrorLiveEvent.postValue(new ApiException(new ApiException.ApiError(400,e.getMessage(),e.getMessage())));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
