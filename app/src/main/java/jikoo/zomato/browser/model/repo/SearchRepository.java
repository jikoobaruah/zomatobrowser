package jikoo.zomato.browser.model.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.text.TextUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import jikoo.zomato.browser.model.dto.response.SearchResponse;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.model.network.ZomatoService;
import jikoo.zomato.browser.utils.SingleLiveEvent;
import retrofit2.Response;

/**
 * Created by jyotishman.baruah on 09/07/18.
 */

public class SearchRepository {

    private static SearchRepository sInstance;


    public static SearchRepository getInstance() {
        if (sInstance == null) {
            sInstance = new SearchRepository();
        }
        return sInstance;
    }


    private static Executor sExecutor = Executors.newSingleThreadExecutor();


    private SingleLiveEvent<SearchResponse> mSearchLiveData;
    private SingleLiveEvent<ApiException> mExceptionLiveData;

    private SearchRepository() {
        mSearchLiveData = new SingleLiveEvent<>();
        mExceptionLiveData = new SingleLiveEvent<>();
    }

    public MutableLiveData<SearchResponse> getSearchLiveData() {
        return mSearchLiveData;
    }

    public MutableLiveData<ApiException> getExceptionLiveData() {
        return mExceptionLiveData;
    }

    public void search(final Context context, String query, final double lat, final double lon, String categories, String cuisines, String collections, String establishments, int start) {
        final HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("q", query);
        queryMap.put("lat", Double.toString(lat));
        queryMap.put("lon", Double.toString(lon));
        queryMap.put("start", Integer.toString(start));
        if (!TextUtils.isEmpty(categories)) {
            queryMap.put("category", categories);
        }
        if (!TextUtils.isEmpty(collections)) {
            queryMap.put("collection_id", collections);
        }
        if (!TextUtils.isEmpty(cuisines)) {
            queryMap.put("cuisines", cuisines);
        }
        if (!TextUtils.isEmpty(establishments)) {
            queryMap.put("establishment_type", establishments);
        }

        sExecutor.execute(new Runnable() {
            @Override
            public void run() {

                try {
                    Response<SearchResponse> response = ZomatoService.ZomatoApi.getInstance(context.getApplicationContext()).getZomatoService().search(queryMap).execute();

                    if (response.isSuccessful()) {
                        mSearchLiveData.postValue(response.body());
                    } else {
                        mExceptionLiveData.postValue(new ApiException(response.errorBody().string()));
                    }
                } catch (IOException e) {
                    mExceptionLiveData.postValue(new ApiException(new ApiException.ApiError(400,e.getMessage(),e.getMessage())));

                }
            }
        });

    }


}
