package jikoo.zomato.browser.model.dto.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Restaurant;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class GeocodeResponse {

    @SerializedName("nearby_restaurants")
    private ArrayList<RestaurantWrapper> mRestaurantWrappers;

    public ArrayList<RestaurantWrapper> getRestaurantWrappers() {
        return mRestaurantWrappers;
    }

    public static class RestaurantWrapper {
        private Restaurant restaurant;

        public Restaurant getRestaurant() {
            return restaurant;
        }
    }
}
