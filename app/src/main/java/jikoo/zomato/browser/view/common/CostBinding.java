package jikoo.zomato.browser.view.common;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import jikoo.zomato.browser.R;


/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class CostBinding {

    @BindingAdapter({"avg_cost","avg_cost_currency"})
    public static void avgCost(TextView textView, String cost, String currency){
       textView.setText(String.format(textView.getContext().getResources().getString(R.string.avg_cost),currency,cost));
    }



}


