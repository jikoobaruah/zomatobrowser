package jikoo.zomato.browser.view.search;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ActivitySearchBinding;
import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.model.dto.response.SearchResponse;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.utils.KeyBoardUtils;
import jikoo.zomato.browser.view.common.FilterManager;
import jikoo.zomato.browser.view.common.PaginationScrollListener;
import jikoo.zomato.browser.view.common.VerticalSpaceItemDecoration;
import jikoo.zomato.browser.view.filter.FilterTitleAdapter;
import jikoo.zomato.browser.viewModel.SearchViewModel;

public class SearchActivity extends AppCompatActivity implements FilterManager.Callbacks {

    private ActivitySearchBinding binding;

    private FilterManager mFilterManager;
    private double mLatitude;
    private double mLongitude;
    private String mAddress;
    private String mQuery;

    private SearchViewModel mSearchViewModel;

    private ArrayList<Restaurant> mRestaurants;

    private boolean mIsLoading;

    private SearchAdapter mAdapter;

    private boolean isProgressVisible;
    private boolean isErrorVisible;


    private boolean mIsLastPageLoaded;


    private Observer<SearchResponse> mSearchObserver = new Observer<SearchResponse>() {
        @Override
        public void onChanged(@Nullable SearchResponse response) {
            hideProgress();
            mIsLoading = false;
            if (mRestaurants == null) {
                mRestaurants = new ArrayList<>();
            }

            if (response.getRestaurants() == null || response.getRestaurants().size() == 0) {
                mAdapter.hideLoadMore();
                mIsLastPageLoaded = true;
                if (mRestaurants.size() == 0){
                    showError(new ApiException(new ApiException.ApiError(400,"No results were found for the given query", "No results were found for the given query")));
                }
            } else {

                ArrayList<Restaurant> restaurants = new ArrayList<>();
                for (SearchResponse.RestaurantWrapper wrapper : response.getRestaurants()) {
                    restaurants.add(wrapper.getRestaurant());
                }
                mRestaurants.addAll(restaurants);
                mAdapter.addAll(restaurants);
            }
        }
    };


    private Observer<ApiException> mErrorObserver = new Observer<ApiException>() {
        @Override
        public void onChanged(@Nullable ApiException e) {
            if (e == null) {
                return;
            }
            hideProgress();
            mAdapter.hideLoadMore();
            if (mRestaurants == null || mRestaurants.size() == 0)
                showError(e);
        }
    };
    private ApiException.ApiError apiError;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLatitude = getIntent().getDoubleExtra(ARGS.LATITUDE, Double.MIN_VALUE);
        mLongitude = getIntent().getDoubleExtra(ARGS.LONGITUDE, Double.MIN_VALUE);
        mAddress = getIntent().getStringExtra(ARGS.ADDRESS);

        mSearchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        mSearchViewModel.getSearchLiveData().observe(this, mSearchObserver);
        mSearchViewModel.getExceptionLiveData().observe(this, mErrorObserver);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mAddress);

        binding.etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!TextUtils.isEmpty(binding.etSearch.getText().toString().trim())) {
                        KeyBoardUtils.hideKeyboard(SearchActivity.this, binding.etSearch);
                        searchFresh();
                    }
                    return true;
                }
                return false;
            }
        });

        mFilterManager = new FilterManager();

        mFilterManager.onCreate(savedInstanceState, binding.filterQuickView, this);

        binding.fabFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFilterManager.isFilterSet()) {
                    mFilterManager.showFilterQuickView(mLatitude, mLongitude);
                } else {
                    mFilterManager.startFilterActivity(SearchActivity.this, FilterTitleAdapter.MODE.CATEGORY, mLatitude, mLongitude);
                }
            }
        });

        binding.rvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.rvList.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) binding.rvList.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                search();

            }

            @Override
            protected boolean isLastPage() {
                return mIsLastPageLoaded;
            }

            @Override
            public boolean isLoading() {
                return mIsLoading;
            }
        });
        binding.rvList.addItemDecoration(new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.margin_small)));
        mAdapter = new SearchAdapter();
        binding.rvList.setAdapter(mAdapter);

        if (savedInstanceState != null) {
            mRestaurants = savedInstanceState.getParcelableArrayList(OUT_STATE.LIST);
            mAdapter.addAll(mRestaurants);
            Parcelable listState = savedInstanceState.getParcelable(OUT_STATE.LIST_STATE);
            binding.rvList.getLayoutManager().onRestoreInstanceState(listState);
            isErrorVisible = savedInstanceState.getBoolean(OUT_STATE.IS_ERROR_VISIBLE);
            isProgressVisible = savedInstanceState.getBoolean(OUT_STATE.IS_PROGRESS_VISIBLE);
            if (isErrorVisible){
                apiError = savedInstanceState.getParcelable(OUT_STATE.ERROR);
                showError(new ApiException(apiError));
            }
            if (isProgressVisible)
                showProgress();
            mQuery = savedInstanceState.getString(OUT_STATE.QUERY,null);
            if (!TextUtils.isEmpty(mQuery)){
                KeyBoardUtils.hideKeyboard(this,binding.etSearch);
            }


        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFilterManager.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mFilterManager.onSaveInstanceState(outState);
        outState.putParcelableArrayList(OUT_STATE.LIST, mRestaurants);
        outState.putParcelable(OUT_STATE.LIST_STATE, binding.rvList.getLayoutManager().onSaveInstanceState());
        outState.putBoolean(OUT_STATE.IS_ERROR_VISIBLE,isErrorVisible);
        if (isErrorVisible)
            outState.putParcelable(OUT_STATE.ERROR,apiError);
        outState.putBoolean(OUT_STATE.IS_PROGRESS_VISIBLE,isProgressVisible);
        outState.putString(OUT_STATE.QUERY,mQuery);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mFilterManager.onActivityResult(requestCode, resultCode, data)) {
            KeyBoardUtils.hideKeyboard(this, binding.etSearch);
            searchFresh();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFilterQuickViewShown() {
        binding.fabFilter.setVisibility(View.GONE);
//        binding.fabSort.setVisibility(View.GONE);

    }

    @Override
    public void onFilterQuickViewHidden() {
        binding.fabFilter.setVisibility(View.VISIBLE);
//        binding.fabSort.setVisibility(View.VISIBLE);
    }

    private void searchFresh() {
        mAdapter.clear();
        if (mRestaurants != null) {
            mRestaurants.clear();
        }
        mIsLastPageLoaded = false;
        showProgress();
        search();
    }

    private void search() {
        mIsLoading = true;
        mQuery = binding.etSearch.getText().toString().trim();

        mSearchViewModel.search(
                mQuery,
                mLatitude,
                mLongitude,
                mFilterManager.getCategories(),
                mFilterManager.getCollections(),
                mFilterManager.getCuisines(),
                mFilterManager.getEstablishments(),
                mRestaurants == null ? 0 : mRestaurants.size()
        );
    }


    private void showError(ApiException e) {
        apiError = e.getError();
        isErrorVisible  = true;
        isProgressVisible = false;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.VISIBLE);
        binding.layoutProgress.progress.setVisibility(View.GONE);
        binding.layoutProgress.ivError.setVisibility(View.VISIBLE);
        binding.layoutProgress.btnRetry.setVisibility(View.VISIBLE);
        binding.layoutProgress.btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchFresh();
            }
        });
        binding.layoutProgress.tvStatus.setText(e.getError().getStatus());
//        binding.layoutProgress.tvMessage.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        isErrorVisible  = false;
        isProgressVisible = true;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.VISIBLE);
        binding.layoutProgress.progress.setVisibility(View.VISIBLE);
        binding.layoutProgress.ivError.setVisibility(View.GONE);
        binding.layoutProgress.btnRetry.setVisibility(View.GONE);
        binding.layoutProgress.tvStatus.setText(getResources().getText(R.string.search_progress));
//        binding.layoutProgress.tvMessage.setVisibility(View.GONE);
    }

    private void hideProgress() {
        isProgressVisible = false;
        isErrorVisible  = false;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.GONE);
    }

    public static class ARGS {
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE = "lon";
        public static final String ADDRESS = "add";
    }

    private class OUT_STATE {
        public static final String LIST = "list";
        public static final String LIST_STATE = "list_state";
        public static final String IS_ERROR_VISIBLE = "iev";
        public static final String ERROR = "e";
        public static final String IS_PROGRESS_VISIBLE = "ipv";
        public static final String QUERY = "QUERY";
    }
}
