package jikoo.zomato.browser.view.details;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Restaurant;

/**
 * Created by jyotishman.baruah on 11/07/18.
 */

public class DetailsPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Restaurant> mRestaurants;
    private Context mContext;

    public DetailsPagerAdapter(Context context,FragmentManager fm, ArrayList<Restaurant> restaurants) {
        super(fm);
        mRestaurants  = restaurants;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return DetailFragment.getFragment(mContext, mRestaurants.get(position));
    }

    @Override
    public int getCount() {
        if (mRestaurants == null)
            return 0;
        return mRestaurants.size();
    }


}
