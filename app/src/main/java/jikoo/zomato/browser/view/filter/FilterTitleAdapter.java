package jikoo.zomato.browser.view.filter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ItemFilterTitleBinding;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class FilterTitleAdapter extends RecyclerView.Adapter<FilterTitleAdapter.ViewHolder> {

    private String[] TITLES = {"CATEGORY", "COLLECTION", "CUISINE", "ESTABLISHMENT"};
    private int[] MODES = {MODE.CATEGORY, MODE.COLLECTION, MODE.CUISINE, MODE.ESTABLISHMENT};
    private int mSelection = 0;

    private Callback mCallback;

    public FilterTitleAdapter(Callback callback){
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFilterTitleBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_filter_title, parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView();
    }


    @Override
    public int getItemCount() {
        return 4;
    }

    public void setMode(int i) {
        int oldMode = mSelection;
        mSelection = i;
        notifyItemChanged(mSelection);
        if (oldMode >= 0) {
            notifyItemChanged(oldMode);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemFilterTitleBinding mBinding;

        public ViewHolder(ItemFilterTitleBinding itemView) {
            super(itemView.getRoot());
            mBinding = itemView;
        }

        public void bindView() {
            mBinding.tvFilterTitle.setText(TITLES[getAdapterPosition()]);
            if (mSelection == getAdapterPosition()) {
                mBinding.tvFilterTitle.setTextScaleX(1.1f);
                mBinding.cvFilterItem.setCardBackgroundColor(mBinding.cvFilterItem.getContext().getResources().getColor(R.color.secondaryTextColor));
            } else {
                mBinding.tvFilterTitle.setTextScaleX(1.0f);
                mBinding.cvFilterItem.setCardBackgroundColor(mBinding.cvFilterItem.getContext().getResources().getColor(R.color.secondaryDarkColor));
            }

            mBinding.cvFilterItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setMode(getAdapterPosition());
                    if (mCallback != null)
                        mCallback.onModeChanged(MODES[getAdapterPosition()]);

                }
            });

        }
    }

    public class MODE {
        public static final int CATEGORY = 145;
        public static final int COLLECTION = 146;
        public static final int CUISINE = 147;
        public static final int ESTABLISHMENT = 148;
    }

    public interface Callback{
        void onModeChanged(int mode);
    }
}
