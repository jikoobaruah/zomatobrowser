package jikoo.zomato.browser.view.main;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ItemRestaurantBinding;
import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.utils.Utils;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class RestaurantAdapter extends android.support.v7.widget.RecyclerView.Adapter<RestaurantAdapter.ViewHolder> {

    private ArrayList<Restaurant> mRestaurants;

    public RestaurantAdapter(){
        mRestaurants = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemRestaurantBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_restaurant, parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView();
    }

    @Override
    public int getItemCount() {
        return  mRestaurants.size();
    }

    public void addAll(ArrayList<Restaurant> restaurants) {
        if (restaurants == null || restaurants.size() == 0)
            return;
        int start = mRestaurants.size();
        mRestaurants.addAll(restaurants);
        notifyItemRangeInserted(start,restaurants.size());
    }

    public ArrayList<Restaurant> getList() {
        return mRestaurants;
    }

    public void clear() {
        int size = mRestaurants.size();
        mRestaurants.clear();
        notifyItemRangeRemoved(0,size);
    }

    public  class  ViewHolder extends RecyclerView.ViewHolder{

        private ItemRestaurantBinding mBinding;

        public ViewHolder(ItemRestaurantBinding itemRestaurantBinding) {
            super(itemRestaurantBinding.getRoot());
            mBinding = itemRestaurantBinding;
        }

        public void bindView() {
            Restaurant restaurant =  mRestaurants.get(getAdapterPosition());
            mBinding.setRestaurant(restaurant);
            mBinding.executePendingBindings();
            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.launchDetailsView(v.getContext(),mRestaurants,getAdapterPosition());
                }
            });

        }
    }
}
