package jikoo.zomato.browser.view.common;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import java.util.ArrayList;

import jikoo.zomato.browser.databinding.LayoutFilterBinding;
import jikoo.zomato.browser.model.dto.Category;
import jikoo.zomato.browser.model.dto.Collection;
import jikoo.zomato.browser.model.dto.Cuisine;
import jikoo.zomato.browser.model.dto.Establishment;
import jikoo.zomato.browser.view.filter.FilterActivity;
import jikoo.zomato.browser.view.filter.FilterTitleAdapter;
import jikoo.zomato.browser.view.search.FilterHorizontalAdapter;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class FilterManager {

    private static final int REQUEST_FILTER = 2342;
    private static final int HORIZONTAL_SPACING = 10;
    private LayoutFilterBinding mBinding;

    private ArrayList<Cuisine> mCuisines;
    private ArrayList<Category> mCategories;
    private ArrayList<Establishment> mEstablishments;
    private ArrayList<Collection> mCollections;
    private boolean mQuickViewVisible;

    private double mLatitude;
    private double mLongitude;

    private Callbacks mCallbacks;

    public void onCreate(Bundle savedInstanceState, LayoutFilterBinding filterQuickView, Callbacks callbacks) {
        mBinding = filterQuickView;
        mCallbacks = callbacks;
        initQuickView();
        if (savedInstanceState == null) {
            return;
        }
        mCuisines = savedInstanceState.getParcelableArrayList(OUTSTATE.CUISINES);
        mCategories = savedInstanceState.getParcelableArrayList(OUTSTATE.CATEGORIES);
        mEstablishments = savedInstanceState.getParcelableArrayList(OUTSTATE.ESTABLISHMENT);
        mCollections = savedInstanceState.getParcelableArrayList(OUTSTATE.COLLECTION);
        mLatitude = savedInstanceState.getDouble(OUTSTATE.LATITUDE);
        mLongitude = savedInstanceState.getDouble(OUTSTATE.LONGITUDE);

        mQuickViewVisible = savedInstanceState.getBoolean(OUTSTATE.IS_QUICK_VIEW_VISIBLE);
        if (mQuickViewVisible) {
            showFilterQuickView(mLatitude, mLongitude);
        }


    }


    public boolean isFilterSet() {
        return (mCuisines != null && mCuisines.size() > 0) ||
                (mCollections != null && mCollections.size() > 0) ||
                (mCategories != null && mCategories.size() > 0) ||
                (mEstablishments != null && mEstablishments.size() > 0);
    }

    public void showFilterQuickView(double lat, double lon) {
        mQuickViewVisible = true;
        mLongitude = lon;
        mLatitude = lat;
        mBinding.getRoot().setVisibility(View.VISIBLE);
        ObjectAnimator anim = ObjectAnimator.ofFloat(mBinding.getRoot(), "translationY", 0);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
        setSectionVisibility(lat, lon);
        if (mCallbacks != null)
            mCallbacks.onFilterQuickViewShown();
    }

    public void startFilterActivity(Activity context, int mode, double lat, double lon) {
        Intent intent = new Intent(context, FilterActivity.class);
        intent.putExtra(FilterActivity.ARGS.MODE, mode);
        intent.putExtra(FilterActivity.ARGS.LATITUDE, lat);
        intent.putExtra(FilterActivity.ARGS.LONGITUDE, lon);
        intent.putExtra(FilterActivity.ARGS.SELECTED_CATEGORIES, mCategories);
        intent.putExtra(FilterActivity.ARGS.SELECTED_COLLECTION, mCollections);
        intent.putExtra(FilterActivity.ARGS.SELECTED_CUISINES, mCuisines);
        intent.putExtra(FilterActivity.ARGS.SELECTED_ESTABLISHMENTS, mEstablishments);
        context.startActivityForResult(intent, REQUEST_FILTER);
    }

    public void onDestroy() {
        mBinding = null;
        mCallbacks = null;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(OUTSTATE.CUISINES, mCuisines);
        outState.putParcelableArrayList(OUTSTATE.CATEGORIES, mCategories);
        outState.putParcelableArrayList(OUTSTATE.ESTABLISHMENT, mEstablishments);
        outState.putParcelableArrayList(OUTSTATE.COLLECTION, mCollections);
        outState.putBoolean(OUTSTATE.IS_QUICK_VIEW_VISIBLE, mQuickViewVisible);

    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQUEST_FILTER) {
            return false;
        }

        if (resultCode == Activity.RESULT_OK && data != null) {
            mCuisines = data.getParcelableArrayListExtra(FilterActivity.ARGS.SELECTED_CUISINES);
            mCategories = data.getParcelableArrayListExtra(FilterActivity.ARGS.SELECTED_CATEGORIES);
            mCollections = data.getParcelableArrayListExtra(FilterActivity.ARGS.SELECTED_COLLECTION);
            mEstablishments = data.getParcelableArrayListExtra(FilterActivity.ARGS.SELECTED_ESTABLISHMENTS);
        }

        return true;
    }

    private void initQuickView() {
        hideQuickView();
        mQuickViewVisible = false;
        mBinding.ivClose.setClickable(true);
        mBinding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideQuickView();
            }
        });
    }

    private void hideQuickView() {
        mBinding.getRoot().setVisibility(View.GONE);
        mBinding.getRoot().setTranslationY(10000);
        mQuickViewVisible = false;
        if (mCallbacks != null)
            mCallbacks.onFilterQuickViewHidden();
    }

    private void setSectionVisibility(double lat, double lon) {
        if (mCategories != null && mCategories.size() > 0) {
            mBinding.layoutFilterCategory.getRoot().setVisibility(View.VISIBLE);
        } else {
            mBinding.layoutFilterCategory.getRoot().setVisibility(View.GONE);
        }

        if (mCollections != null && mCollections.size() > 0) {
            mBinding.layoutFilterCollections.getRoot().setVisibility(View.VISIBLE);
        } else {
            mBinding.layoutFilterCollections.getRoot().setVisibility(View.GONE);
        }

        if (mCuisines != null && mCuisines.size() > 0) {
            mBinding.layoutFilterCuisine.getRoot().setVisibility(View.VISIBLE);
        } else {
            mBinding.layoutFilterCuisine.getRoot().setVisibility(View.GONE);
        }

        if (mEstablishments != null && mEstablishments.size() > 0) {
            mBinding.layoutFilterEstablishment.getRoot().setVisibility(View.VISIBLE);
        } else {
            mBinding.layoutFilterEstablishment.getRoot().setVisibility(View.GONE);
        }

        setSectionViews(lat, lon);
    }


    private void setSectionViews(double lat, double lon) {
        mLatitude = lat;
        mLongitude = lon;
        if (mCategories != null && mCategories.size() > 0) {
            mBinding.layoutFilterCategory.getRoot().setClickable(true);
            mBinding.layoutFilterCategory.getRoot().setOnClickListener(new FilterClickListener(FilterTitleAdapter.MODE.CATEGORY, lat, lon));
            mBinding.layoutFilterCategory.tvFilterTitle.setText("Category");
            mBinding.layoutFilterCategory.rvList.setLayoutManager(new LinearLayoutManager(mBinding.layoutFilterCategory.rvList.getContext(), LinearLayoutManager.HORIZONTAL, false));
            mBinding.layoutFilterCategory.rvList.addItemDecoration(new HorizontalSpaceItemDecoration(HORIZONTAL_SPACING));
            FilterHorizontalAdapter<Category> categoryFilterAdapter = new FilterHorizontalAdapter<>();
            categoryFilterAdapter.addAll(mCategories);
            mBinding.layoutFilterCategory.rvList.setAdapter(categoryFilterAdapter);
        }

        if (mCollections != null && mCollections.size() > 0) {
            mBinding.layoutFilterCollections.getRoot().setClickable(true);
            mBinding.layoutFilterCollections.getRoot().setOnClickListener(new FilterClickListener(FilterTitleAdapter.MODE.COLLECTION, lat, lon));
            mBinding.layoutFilterCollections.tvFilterTitle.setText("Collection");
            mBinding.layoutFilterCollections.rvList.setLayoutManager(new LinearLayoutManager(mBinding.layoutFilterCollections.rvList.getContext(), LinearLayoutManager.HORIZONTAL, false));
            mBinding.layoutFilterCollections.rvList.addItemDecoration(new HorizontalSpaceItemDecoration(HORIZONTAL_SPACING));
            FilterHorizontalAdapter<Collection> collectionFilterAdapter = new FilterHorizontalAdapter<>();
            collectionFilterAdapter.addAll(mCollections);
            mBinding.layoutFilterCollections.rvList.setAdapter(collectionFilterAdapter);
        }

        if (mCuisines != null && mCuisines.size() > 0) {
            mBinding.layoutFilterCuisine.getRoot().setClickable(true);
            mBinding.layoutFilterCuisine.getRoot().setOnClickListener(new FilterClickListener(FilterTitleAdapter.MODE.CUISINE, lat, lon));
            mBinding.layoutFilterCuisine.tvFilterTitle.setText("Cuisine");
            mBinding.layoutFilterCuisine.rvList.setLayoutManager(new LinearLayoutManager(mBinding.layoutFilterCuisine.rvList.getContext(), LinearLayoutManager.HORIZONTAL, false));
            mBinding.layoutFilterCuisine.rvList.addItemDecoration(new HorizontalSpaceItemDecoration(HORIZONTAL_SPACING));
            FilterHorizontalAdapter<Cuisine> cuisineFilterAdapter = new FilterHorizontalAdapter<>();
            cuisineFilterAdapter.addAll(mCuisines);
            mBinding.layoutFilterCuisine.rvList.setAdapter(cuisineFilterAdapter);
        }

        if (mEstablishments != null && mEstablishments.size() > 0) {
            mBinding.layoutFilterEstablishment.getRoot().setClickable(true);
            mBinding.layoutFilterEstablishment.getRoot().setOnClickListener(new FilterClickListener(FilterTitleAdapter.MODE.ESTABLISHMENT, lat, lon));
            mBinding.layoutFilterEstablishment.tvFilterTitle.setText("Establishment");
            mBinding.layoutFilterEstablishment.rvList.setLayoutManager(new LinearLayoutManager(mBinding.layoutFilterEstablishment.rvList.getContext(), LinearLayoutManager.HORIZONTAL, false));
            mBinding.layoutFilterEstablishment.rvList.addItemDecoration(new HorizontalSpaceItemDecoration(HORIZONTAL_SPACING));
            FilterHorizontalAdapter<Establishment> establishmentFilterAdapter = new FilterHorizontalAdapter<>();
            establishmentFilterAdapter.addAll(mEstablishments);
            mBinding.layoutFilterEstablishment.rvList.setAdapter(establishmentFilterAdapter);
        }
    }


    public String getCategories() {
        if (mCategories == null || mCategories.size() == 0)
            return null;
        StringBuilder stringBuilder = new StringBuilder();
        for (Category category : mCategories){
            stringBuilder.append(category.getId()).append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }

    public String getCollections() {
        if (mCollections == null || mCollections.size() == 0)
            return null;
        StringBuilder stringBuilder = new StringBuilder();
        for (Collection collection : mCollections){
            stringBuilder.append(collection.getId()).append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }

    public String getCuisines() {
        if (mCuisines == null || mCuisines.size() == 0)
            return null;
        StringBuilder stringBuilder = new StringBuilder();
        for (Cuisine cuisine : mCuisines){
            stringBuilder.append(cuisine.getId()).append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }

    public String getEstablishments() {
        if (mEstablishments == null || mEstablishments.size() == 0)
            return null;
        StringBuilder stringBuilder = new StringBuilder();
        for (Establishment establishment : mEstablishments){
            stringBuilder.append(establishment.getId()).append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }

    private class OUTSTATE {
        public static final String CUISINES = "cuisines";
        public static final String CATEGORIES = "categories";
        public static final String ESTABLISHMENT = "establishment";
        public static final String COLLECTION = "collection";
        public static final String IS_QUICK_VIEW_VISIBLE = "iqvv";
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE = "lon";
    }

    private class FilterClickListener implements View.OnClickListener {

        private int mMode = FilterTitleAdapter.MODE.CATEGORY;
        private double mLatitude;
        private double mLongitude;

        public FilterClickListener(int mode, double latitude, double longitude) {
            mMode = mode;
            mLatitude = latitude;
            mLongitude = longitude;
        }

        @Override
        public void onClick(View v) {
            hideQuickView();
            startFilterActivity((Activity) v.getContext(),mMode, mLatitude, mLongitude);
        }
    }

    public interface Callbacks{

        void onFilterQuickViewShown();

        void onFilterQuickViewHidden();
    }
}
