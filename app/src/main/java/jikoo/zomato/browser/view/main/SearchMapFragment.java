package jikoo.zomato.browser.view.main;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.FragmentSearchMapBinding;
import jikoo.zomato.browser.model.dto.Restaurant;

/**
 * Created by jyotishman.baruah on 04/07/18.
 */

public class SearchMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener {

    private static final String TAG = SearchMapFragment.class.getSimpleName();

    private ArrayList<Restaurant> restaurants;
    private CallBack callBack;

    private HashMap<Marker, Restaurant> markersList;
    private double longitude;
    private double latitude;
    private GoogleMap mMap;
    private FragmentSearchMapBinding binding;


    public static Fragment getFragment(Context context, ArrayList<Restaurant> restaurants, double lat, double lon) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARGS.LIST, restaurants);
        args.putDouble(ARGS.LAT, lat);
        args.putDouble(ARGS.LON, lon);
        return SearchMapFragment.instantiate(context, SearchMapFragment.class.getName(), args);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        if (context instanceof SearchListFragment.CallBack) {
            callBack = (CallBack) context;
        } else {
            throw new RuntimeException("Activity must implement " + CallBack.class.getName());
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        latitude = getArguments().getDouble(ARGS.LAT);
        longitude = getArguments().getDouble(ARGS.LON);
        restaurants = getArguments().getParcelableArrayList(ARGS.LIST);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_search_map,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");

        FragmentManager fm = getChildFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) fm.findFragmentByTag("mapFragment");
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.map, mapFragment, "mapFragment");
            ft.commit();
            fm.executePendingTransactions();
        }
        mapFragment.getMapAsync(this);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callBack != null) {
                    callBack.gotoListView(getArguments().<Restaurant>getParcelableArrayList(SearchMapFragment.ARGS.LIST));
                }
            }
        });

        binding.cvSearchHere.setVisibility(View.GONE);
        binding.cvSearchHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMap!= null) {
                    Toast.makeText(getContext(),mMap.getCameraPosition().target.toString(),Toast.LENGTH_SHORT).show();
                    if (callBack != null)
                        callBack.onMapLocationChanged(mMap.getCameraPosition().target);
                }
            }
        });
    }


    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach");
        callBack = null;
    }

    private LatLngBounds mLatLngBounds;

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        mMap = googleMap;


        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (Restaurant restaurant : restaurants) {
                    if (restaurant.getLocation().getLatLng().latitude != 0 || restaurant.getLocation().getLatLng().longitude != 0)
                        boundsBuilder.include(restaurant.getLocation().getLatLng());
                }

                mLatLngBounds = boundsBuilder.build();
                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(mLatLngBounds, (int) (getView().getWidth() * 0.12)), new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        markersList = new HashMap<>();
                        for (Restaurant restaurant : restaurants) {
                            markersList.put(googleMap.addMarker(
                                    new MarkerOptions().
                                            title(restaurant.getName()).
                                            icon(BitmapDescriptorFactory.fromBitmap(getBitmapForRestaurant(restaurant.getUserRating()))).
                                            snippet(restaurant.getUserRating().getAggregateRating()).
                                            anchor(0, 1).
                                            position(restaurant.getLocation().getLatLng())), restaurant);
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });

            }
        });


        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnCameraIdleListener(this);

    }

    private Bitmap getBitmapForRestaurant(Restaurant.UserRating userRating) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_chat_bubble_black_72dp);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }


        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth() + 5, drawable.getIntrinsicHeight() + 5, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());

        DrawableCompat.setTint(drawable, Color.WHITE);
        drawable.draw(canvas);

        DrawableCompat.setTint(drawable, Color.parseColor(userRating.getRatingColor()));
        drawable.setAlpha(200);
        drawable.setBounds(5, 5, canvas.getWidth() - 5, canvas.getHeight() - 5);
        drawable.draw(canvas);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(40);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        TypedValue typedValue = new TypedValue();
        TypedArray a = getContext().obtainStyledAttributes(typedValue.data, new int[]{R.attr.colorAccent});
        int color = a.getColor(0, canvas.getDensity());
        a.recycle();
        paint.setColor(color);

        Rect bounds = new Rect();
        paint.getTextBounds(userRating.getAggregateRating(), 0, userRating.getAggregateRating().length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y = bitmap.getHeight() / 2;


        canvas.drawText(userRating.getAggregateRating(), x, y, paint);


        return bitmap;
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        BottomSheetFragment bottomSheetFragment = BottomSheetFragment.getFragment(getContext(), markersList.get(marker));
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
        return true;
    }

    @Override
    public void onCameraIdle() {
        LatLngBounds currentBounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        boolean arePointsVisibleInMap = false;
        LatLng restaurantLatLng;
        for (Restaurant restaurant : restaurants) {
            arePointsVisibleInMap = arePointsVisibleInMap || currentBounds.contains(restaurant.getLocation().getLatLng());
        }


        if (arePointsVisibleInMap) {
            binding.cvSearchHere.setVisibility(View.GONE);
            Log.d(TAG, "Points are visible in map");
        } else {
            binding.cvSearchHere.setVisibility(View.VISIBLE);
            Log.d(TAG, "Points are not visible in map");
        }
    }


    private static class ARGS {
        public static final String LIST = "list";
        public static final String LON = "lon";
        public static String LAT = "lat";
    }


    public interface CallBack {
        void gotoListView(ArrayList<Restaurant> restaurants);

        void onMapLocationChanged(LatLng target);
    }
}
