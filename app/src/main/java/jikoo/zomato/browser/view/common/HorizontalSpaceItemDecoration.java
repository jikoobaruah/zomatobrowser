package jikoo.zomato.browser.view.common;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

class HorizontalSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int horizontalSpaceHeight;

    public HorizontalSpaceItemDecoration(int horizontalSpaceHeight) {
        this.horizontalSpaceHeight = horizontalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = horizontalSpaceHeight;

//        if (parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
//            outRect.right = horizontalSpaceHeight;
//        }
    }
}
