package jikoo.zomato.browser.view.common;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import jikoo.zomato.browser.R;


/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class ImageBinding {

    @BindingAdapter("image_binder")
    public static void bindImage(ImageView imageView, String url){
        GlideApp.with(imageView.getContext())
                .load(url)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.restaurant)
                .into(imageView);
    }

    @BindingAdapter("image_binder_xy")
    public static void bindImageXY(ImageView imageView, String url){
        GlideApp.with(imageView.getContext())
                .load(url)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .placeholder(R.drawable.restaurant)
                .into(imageView);
    }


}


