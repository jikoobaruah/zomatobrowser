package jikoo.zomato.browser.view.filter;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ActivityFilterBinding;
import jikoo.zomato.browser.model.dto.Category;
import jikoo.zomato.browser.model.dto.Collection;
import jikoo.zomato.browser.model.dto.Cuisine;
import jikoo.zomato.browser.model.dto.Establishment;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.view.common.VerticalSpaceItemDecoration;
import jikoo.zomato.browser.viewModel.CategoryViewModel;
import jikoo.zomato.browser.viewModel.CollectionViewModel;
import jikoo.zomato.browser.viewModel.CuisineViewModel;
import jikoo.zomato.browser.viewModel.EstablishmentViewModel;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class FilterActivity extends AppCompatActivity implements FilterTitleAdapter.Callback {

    private CuisineViewModel mCuisineViewModel;
    private CategoryViewModel mCategoryViewModel;
    private CollectionViewModel mCollectionViewModel;
    private EstablishmentViewModel mEstablishmentViewModel;
    private ActivityFilterBinding binding;

    private FilterTitleAdapter mAdapter;
    private int mMode;

    private ArrayList<Category> mCategories;
    private ArrayList<Cuisine> mCuisines;
    private ArrayList<Collection> mCollections;
    private ArrayList<Establishment> mEstablishments;

    private ArrayList<Category> mSelectedCategories;
    private ArrayList<Cuisine> mSelectedCuisines;
    private ArrayList<Collection> mSelectedCollections;
    private ArrayList<Establishment> mSelectedEstablishments;

    private Observer<ApiException> mExceptionObserver = new Observer<ApiException>() {
        @Override
        public void onChanged(@Nullable ApiException e) {
            hideProgress();
            showError(e);

        }
    };

    private Observer<ArrayList<Category>> mCategoryObserver = new Observer<ArrayList<Category>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Category> categories) {

            mCategories = categories;
            if (mMode != FilterTitleAdapter.MODE.CATEGORY) {
                return;
            }
            hideProgress();
            setCategoriesAdapter();
        }
    };

    private Observer<ArrayList<Collection>> mCollectionObserver = new Observer<ArrayList<Collection>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Collection> collections) {

            mCollections = collections;
            if (mMode != FilterTitleAdapter.MODE.COLLECTION) {
                return;
            }
            hideProgress();
            setCollectionsAdapter();

        }
    };


    private Observer<ArrayList<Cuisine>> mCuisineObserver = new Observer<ArrayList<Cuisine>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Cuisine> cuisines) {

            mCuisines = cuisines;
            if (mMode != FilterTitleAdapter.MODE.CUISINE) {
                return;
            }
            hideProgress();
            setCuisinesAdapter();

        }
    };

    private Observer<ArrayList<Establishment>> mEstablishmentObserver = new Observer<ArrayList<Establishment>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Establishment> establishments) {

            mEstablishments = establishments;
            if (mMode != FilterTitleAdapter.MODE.ESTABLISHMENT) {
                return;
            }
            hideProgress();
            setEstablishmentsAdapter();
        }
    };
    private double mLatitude;
    private double mLongitude;

    private ApiException.ApiError apiError;
    private boolean isErrorVisible;
    private boolean isProgressVisible;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mCategoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        mCuisineViewModel = ViewModelProviders.of(this).get(CuisineViewModel.class);
        mCollectionViewModel = ViewModelProviders.of(this).get(CollectionViewModel.class);
        mEstablishmentViewModel = ViewModelProviders.of(this).get(EstablishmentViewModel.class);

        mCategoryViewModel.getLiveData().observe(this, mCategoryObserver);
        mCuisineViewModel.getLiveData().observe(this, mCuisineObserver);
        mCollectionViewModel.getLiveData().observe(this, mCollectionObserver);
        mEstablishmentViewModel.getLiveData().observe(this, mEstablishmentObserver);

        mCategoryViewModel.getErrorLiveData().observe(this, mExceptionObserver);
        mCuisineViewModel.getErrorLiveData().observe(this, mExceptionObserver);
        mCollectionViewModel.getErrorLiveData().observe(this, mExceptionObserver);
        mEstablishmentViewModel.getErrorLiveData().observe(this, mExceptionObserver);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_filter);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_filter));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAdapter = new FilterTitleAdapter(this);

        binding.rvFilter.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.rvFilter.addItemDecoration(new VerticalSpaceItemDecoration(10));
        binding.rvFilter.setAdapter(mAdapter);

        binding.rvFilterList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.rvFilterList.addItemDecoration(new VerticalSpaceItemDecoration(10));

        if (savedInstanceState == null) {
            mMode = getIntent().getIntExtra(ARGS.MODE, FilterTitleAdapter.MODE.CATEGORY);
            mLatitude = getIntent().getDoubleExtra(ARGS.LATITUDE, Double.MIN_VALUE);
            mLongitude = getIntent().getDoubleExtra(ARGS.LONGITUDE, Double.MIN_VALUE);

            mSelectedCategories = getIntent().getParcelableArrayListExtra(ARGS.SELECTED_CATEGORIES);
            mSelectedCollections = getIntent().getParcelableArrayListExtra(ARGS.SELECTED_COLLECTION);
            mSelectedCuisines = getIntent().getParcelableArrayListExtra(ARGS.SELECTED_CUISINES);
            mSelectedEstablishments = getIntent().getParcelableArrayListExtra(ARGS.SELECTED_ESTABLISHMENTS);
            onModeChanged(mMode);

        } else {
            mMode = savedInstanceState.getInt(ARGS.MODE, FilterTitleAdapter.MODE.CATEGORY);
            mLatitude = savedInstanceState.getDouble(ARGS.LATITUDE, Double.MIN_VALUE);
            mLongitude = savedInstanceState.getDouble(ARGS.LONGITUDE, Double.MIN_VALUE);

            mSelectedCategories = savedInstanceState.getParcelableArrayList(ARGS.SELECTED_CATEGORIES);
            mSelectedCollections = savedInstanceState.getParcelableArrayList(ARGS.SELECTED_COLLECTION);
            mSelectedCuisines = savedInstanceState.getParcelableArrayList(ARGS.SELECTED_CUISINES);
            mSelectedEstablishments = savedInstanceState.getParcelableArrayList(ARGS.SELECTED_ESTABLISHMENTS);

            mCategories = savedInstanceState.getParcelableArrayList(ARGS.CATEGORIES);
            mCollections = savedInstanceState.getParcelableArrayList(ARGS.COLLECTION);
            mCuisines = savedInstanceState.getParcelableArrayList(ARGS.CUISINES);
            mEstablishments = savedInstanceState.getParcelableArrayList(ARGS.ESTABLISHMENT);

            isErrorVisible = savedInstanceState.getBoolean(ARGS.IS_ERROR_VISIBLE);
            isProgressVisible = savedInstanceState.getBoolean(ARGS.IS_PROGRESS_VISIBLE);
            apiError = savedInstanceState.getParcelable(ARGS.ERROR);
            if (isErrorVisible) {
                showError(new ApiException(apiError));
            } else if (isProgressVisible) {
                showProgress();
            } else {
                hideProgress();
                onModeChanged(mMode);
            }
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ARGS.MODE, mMode);
        outState.putDouble(ARGS.LATITUDE, mLatitude);
        outState.putDouble(ARGS.LONGITUDE, mLongitude);


        outState.putParcelableArrayList(ARGS.CATEGORIES, mCategories);
        outState.putParcelableArrayList(ARGS.SELECTED_CATEGORIES, mSelectedCategories);

        outState.putParcelableArrayList(ARGS.COLLECTION, mCollections);
        outState.putParcelableArrayList(ARGS.SELECTED_COLLECTION, mSelectedCollections);

        outState.putParcelableArrayList(ARGS.CUISINES, mCuisines);
        outState.putParcelableArrayList(ARGS.SELECTED_CUISINES, mSelectedCuisines);

        outState.putParcelableArrayList(ARGS.ESTABLISHMENT, mEstablishments);
        outState.putParcelableArrayList(ARGS.SELECTED_ESTABLISHMENTS, mSelectedEstablishments);

        outState.putBoolean(ARGS.IS_ERROR_VISIBLE, isErrorVisible);
        outState.putBoolean(ARGS.IS_PROGRESS_VISIBLE, isErrorVisible);
        outState.putParcelable(ARGS.ERROR, apiError);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            Intent result = new Intent();
            result.putExtra(ARGS.SELECTED_CATEGORIES, mSelectedCategories);
            result.putExtra(ARGS.SELECTED_COLLECTION, mSelectedCollections);
            result.putExtra(ARGS.SELECTED_CUISINES, mSelectedCuisines);
            result.putExtra(ARGS.SELECTED_ESTABLISHMENTS, mSelectedEstablishments);
            setResult(Activity.RESULT_OK, result);
            finish();
            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onModeChanged(int mode) {
        mMode = mode;
        switch (mode) {
            default:
            case FilterTitleAdapter.MODE.CATEGORY:
                mAdapter.setMode(0);
                if (mCategories != null && mCategories.size() > 0) {
                    setCategoriesAdapter();
                } else {
                    showProgress();
                    binding.rvFilterList.setAdapter(null);
                    mCategoryViewModel.fetch();
                }
                break;
            case FilterTitleAdapter.MODE.COLLECTION:
                mAdapter.setMode(1);
                if (mCollections != null && mCollections.size() > 0) {
                    setCollectionsAdapter();

                } else {
                    showProgress();
                    binding.rvFilterList.setAdapter(null);
                    mCollectionViewModel.fetch(mLatitude, mLongitude);

                }
                break;
            case FilterTitleAdapter.MODE.CUISINE:
                mAdapter.setMode(2);
                if (mCuisines != null && mCuisines.size() > 0) {
                    setCuisinesAdapter();

                } else {
                    showProgress();
                    binding.rvFilterList.setAdapter(null);
                    mCuisineViewModel.fetch(mLatitude, mLongitude);

                }
                break;
            case FilterTitleAdapter.MODE.ESTABLISHMENT:
                mAdapter.setMode(3);
                if (mEstablishments != null && mEstablishments.size() > 0) {
                    setEstablishmentsAdapter();
                } else {
                    showProgress();
                    binding.rvFilterList.setAdapter(null);
                    mEstablishmentViewModel.fetch(mLatitude, mLongitude);

                }
                break;

        }
    }

    private void setCategoriesAdapter() {
        hideProgress();
        FilterVerticalAdapter<Category> adapter = new FilterVerticalAdapter(mSelectedCategories, new FilterVerticalAdapter.Callbacks<Category>() {
            @Override
            public void onSelected(Category filter) {
                if (mSelectedCategories == null) {
                    mSelectedCategories = new ArrayList<>();
                }
                if (mSelectedCategories.contains(filter)) {
                    return;
                }
                mSelectedCategories.add(filter);

            }

            @Override
            public void onUnselected(Category filter) {
                if (mSelectedCategories == null) {
                    return;
                }
                mSelectedCategories.remove(filter);

            }
        });
        binding.rvFilterList.setAdapter(adapter);
        adapter.addAll(mCategories);
    }

    private void setCollectionsAdapter() {
        hideProgress();
        FilterVerticalAdapter<Collection> adapter = new FilterVerticalAdapter(mSelectedCollections, new FilterVerticalAdapter.Callbacks<Collection>() {
            @Override
            public void onSelected(Collection filter) {
                if (mSelectedCollections == null) {
                    mSelectedCollections = new ArrayList<>();
                }
                if (mSelectedCollections.contains(filter)) {
                    return;
                }
                mSelectedCollections.add(filter);

            }

            @Override
            public void onUnselected(Collection filter) {
                if (mSelectedCollections == null) {
                    return;
                }
                mSelectedCollections.remove(filter);
            }
        });
        binding.rvFilterList.setAdapter(adapter);
        adapter.addAll(mCollections);
    }

    private void setCuisinesAdapter() {
        hideProgress();
        FilterVerticalAdapter<Cuisine> adapter = new FilterVerticalAdapter(mSelectedCuisines, new FilterVerticalAdapter.Callbacks<Cuisine>() {
            @Override
            public void onSelected(Cuisine filter) {
                if (mSelectedCuisines == null) {
                    mSelectedCuisines = new ArrayList<>();
                }
                if (mSelectedCuisines.contains(filter)) {
                    return;
                }
                mSelectedCuisines.add(filter);
            }

            @Override
            public void onUnselected(Cuisine filter) {
                if (mSelectedCuisines == null) {
                    return;
                }
                mSelectedCuisines.remove(filter);
            }
        });
        binding.rvFilterList.setAdapter(adapter);
        adapter.addAll(mCuisines);
    }

    private void setEstablishmentsAdapter() {
        hideProgress();
        FilterVerticalAdapter<Establishment> adapter = new FilterVerticalAdapter(mSelectedEstablishments, new FilterVerticalAdapter.Callbacks<Establishment>() {
            @Override
            public void onSelected(Establishment filter) {
                if (mSelectedEstablishments == null) {
                    mSelectedEstablishments = new ArrayList<>();
                }
                if (mSelectedEstablishments.contains(filter)) {
                    return;
                }
                mSelectedEstablishments.add(filter);
            }

            @Override
            public void onUnselected(Establishment filter) {
                if (mSelectedEstablishments == null) {
                    return;
                }
                mSelectedEstablishments.remove(filter);
            }
        });
        binding.rvFilterList.setAdapter(adapter);
        adapter.addAll(mEstablishments);
    }

    private void showError(ApiException e) {
        apiError = e.getError();
        isErrorVisible = true;
        isProgressVisible = false;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.VISIBLE);
        binding.layoutProgress.progress.setVisibility(View.GONE);
        binding.layoutProgress.ivError.setVisibility(View.VISIBLE);
        binding.layoutProgress.btnRetry.setVisibility(View.VISIBLE);
        binding.layoutProgress.btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onModeChanged(mMode);
            }
        });
        binding.layoutProgress.tvStatus.setText(e.getError().getStatus());
//        binding.layoutProgress.tvMessage.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        isErrorVisible = false;
        isProgressVisible = true;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.VISIBLE);
        binding.layoutProgress.progress.setVisibility(View.VISIBLE);
        binding.layoutProgress.ivError.setVisibility(View.GONE);
        binding.layoutProgress.btnRetry.setVisibility(View.GONE);
        binding.layoutProgress.tvStatus.setText(getResources().getText(R.string.search_progress));
//        binding.layoutProgress.tvMessage.setVisibility(View.GONE);
    }

    private void hideProgress() {
        isProgressVisible = false;
        isErrorVisible = false;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.GONE);
    }


    public class ARGS {
        public static final String MODE = "mode";
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE = "lon";
        public static final String CUISINES = "cuisines";
        public static final String CATEGORIES = "categories";
        public static final String ESTABLISHMENT = "establishment";
        public static final String COLLECTION = "collection";
        public static final String SELECTED_CATEGORIES = "selected_categories";
        public static final String SELECTED_COLLECTION = "selected_collection";
        public static final String SELECTED_CUISINES = "selected_cuisines";
        public static final String SELECTED_ESTABLISHMENTS = "selected_establishments";
        public static final String IS_ERROR_VISIBLE = "iev";
        public static final String IS_PROGRESS_VISIBLE = "ipv";
        public static final String ERROR = "error";
    }
}
