package jikoo.zomato.browser.view.filter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ItemFilterSelectionBinding;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class FilterVerticalAdapter<T> extends RecyclerView.Adapter<FilterVerticalAdapter.ViewHolder> {

    private ArrayList<T> filters;

    private ArrayList<T> mPresetFiters;

    private Callbacks<T> mCallbacks;

    public FilterVerticalAdapter(ArrayList<T> presetFiters, Callbacks<T> callbacks) {
        filters = new ArrayList<>();
        mCallbacks = callbacks;
        mPresetFiters = presetFiters;
    }

    @NonNull
    @Override
    public FilterVerticalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFilterSelectionBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_filter_selection, parent, false);
        return new ViewHolder(itemBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull FilterVerticalAdapter.ViewHolder holder, int position) {
        holder.bindView();
    }


    @Override
    public int getItemCount() {
        return filters.size();
    }

    public void addAll(ArrayList<T> filters) {
        if (filters == null || filters.size() == 0) {
            return;
        }
        int start = this.filters.size();
        this.filters.addAll(filters);
        notifyItemRangeChanged(start, filters.size());

    }

    public void clear() {
        if (filters == null)
            return;
        filters.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemFilterSelectionBinding mBinding;

        public ViewHolder(ItemFilterSelectionBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bindView() {
            if (mPresetFiters!= null && mPresetFiters.contains(filters.get(getAdapterPosition())))
                mBinding.cbFilter.setChecked(true);
            mBinding.cbFilter.setText(filters.get(getAdapterPosition()).toString());
            mBinding.cbFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked && mCallbacks != null) {
                        mCallbacks.onSelected(filters.get(getAdapterPosition()));
                    } else if (mCallbacks != null) {
                        mCallbacks.onUnselected(filters.get(getAdapterPosition()));
                    }

                }
            });

        }

    }

    public interface Callbacks<T> {
        void onSelected(T filter);

        void onUnselected(T filter);
    }

}
