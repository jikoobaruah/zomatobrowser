package jikoo.zomato.browser.view.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ActivityDetailsBinding;
import jikoo.zomato.browser.model.dto.Restaurant;

public class DetailsActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private DetailsPagerAdapter mSectionsPagerAdapter;

    private ArrayList<Restaurant> mRestaurants;

    private int mSelectionPosition;

    private ActivityDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRestaurants = getIntent().getParcelableArrayListExtra(ARGS.LIST);
        mSelectionPosition = getIntent().getIntExtra(ARGS.POSITION, 0);
        if (savedInstanceState != null){
            mSelectionPosition = savedInstanceState.getInt(ARGS.POSITION, 0);
        }
        if (mRestaurants == null)
            finish();

        setTitle("");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSectionsPagerAdapter = new DetailsPagerAdapter(this , getSupportFragmentManager(), mRestaurants);

        binding.container.setAdapter(mSectionsPagerAdapter);

        binding.container.setCurrentItem(mSelectionPosition);
        onPageSelected(mSelectionPosition);
        binding.container.addOnPageChangeListener(this);

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ARGS.POSITION,mSelectionPosition);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mSelectionPosition = position;
//        setTitle(mRestaurants.get(position).getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class ARGS {
        public static final String LIST = "list";
        public static final String POSITION = "position";
    }
}
