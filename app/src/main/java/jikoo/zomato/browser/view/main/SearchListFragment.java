package jikoo.zomato.browser.view.main;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.FragmentSearchBinding;
import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.view.common.VerticalSpaceItemDecoration;

/**
 * Created by jyotishman.baruah on 04/07/18.
 */

public class SearchListFragment extends Fragment {

    private static final String TAG = SearchListFragment.class.getSimpleName();

    private CallBack callBack;
    private FragmentSearchBinding binding;
    private RestaurantAdapter mAdapter;

    public static Fragment getFragment(Context context, ArrayList<Restaurant> restaurants) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARGS.LIST, restaurants);
        return SearchListFragment.instantiate(context,SearchListFragment.class.getName(),args);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CallBack ){
            callBack = (CallBack) context;
        }else {
            throw  new RuntimeException("Activity must implement "+CallBack.class.getName());
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new RestaurantAdapter();
        mAdapter.addAll(getArguments().<Restaurant>getParcelableArrayList(ARGS.LIST));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_search,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callBack != null)
                    callBack.gotoMapView(mAdapter.getList());
            }
        });

        binding.rvList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        binding.rvList.addItemDecoration(new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.margin_small)));
        binding.rvList.setAdapter(mAdapter);

    }


    @Override
    public void onDetach() {
        super.onDetach();
        callBack = null;
    }




    private static class ARGS {
        public static final String LIST = "list";
    }

    public interface CallBack {
        void gotoMapView(ArrayList<Restaurant> restaurants);
    }
}
