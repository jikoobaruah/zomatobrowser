package jikoo.zomato.browser.view.details;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.FragmentDetailsBinding;
import jikoo.zomato.browser.model.dto.Restaurant;

/**
 * Created by jyotishman.baruah on 11/07/18.
 */

public class DetailFragment extends Fragment {

    public static Fragment getFragment(Context context,Restaurant restaurant) {
        Bundle args = new Bundle();
        args.putParcelable(ARGS.ITEM , restaurant);
        return Fragment.instantiate(context,DetailFragment.class.getName(),args);
    }

    private FragmentDetailsBinding binding;
    private Restaurant mRestaurant;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRestaurant = getArguments().getParcelable(ARGS.ITEM);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details,container,false);
        return binding.getRoot();
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setRestaurant(mRestaurant);
    }



    public static class ARGS{
        public static final String ITEM = "item";
    }
}
