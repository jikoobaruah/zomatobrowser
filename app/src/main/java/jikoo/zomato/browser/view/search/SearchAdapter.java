package jikoo.zomato.browser.view.search;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ItemLoadMoreBinding;
import jikoo.zomato.browser.databinding.ItemRestaurantBinding;
import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.utils.Utils;

/**
 * Created by jyotishman.baruah on 09/07/18.
 */

class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private static final int ITEM_TYPE_LOAD_MORE = 1;
    private static final int ITEM_TYPE_ITEM = 2;
    private ArrayList<Restaurant> mRestaurants;

    private boolean mShowLoadingMode;

    public SearchAdapter() {
        mRestaurants = new ArrayList<>();
        mShowLoadingMode = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE_ITEM) {
            ItemRestaurantBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_restaurant, parent, false);
            return new ItemHolder(itemBinding);
        } else {
            ItemLoadMoreBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_load_more, parent, false);
            return new LoadMoreHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mRestaurants.size()) {
            return ITEM_TYPE_LOAD_MORE;
        }
        return ITEM_TYPE_ITEM;

    }

    @Override
    public int getItemCount() {
        return mRestaurants.size() + (mShowLoadingMode ? 1 : 0);
    }


    public void addAll(ArrayList<Restaurant> restaurants) {
        if (restaurants == null || restaurants.size() == 0) {
            return;
        }
        mShowLoadingMode = true;
        int start = mRestaurants.size();
        mRestaurants.addAll(restaurants);
        notifyItemRangeInserted(start, restaurants.size());
    }


    public void clear() {
        mShowLoadingMode = false;
        mRestaurants.clear();
        notifyDataSetChanged();
    }

    public void hideLoadMore() {
        mShowLoadingMode = false;
        notifyItemRemoved(mRestaurants.size());
    }

    public abstract class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View view) {
            super(view);
        }

        public abstract void bindView();


    }

    public class ItemHolder extends ViewHolder {

        private ItemRestaurantBinding mBinding;


        public ItemHolder(ItemRestaurantBinding itemRestaurantBinding) {
            super(itemRestaurantBinding.getRoot());
            mBinding = itemRestaurantBinding;

        }

        public void bindView() {
            Restaurant restaurant = mRestaurants.get(getAdapterPosition());
            mBinding.setRestaurant(restaurant);
            mBinding.executePendingBindings();
            mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.launchDetailsView(v.getContext(),mRestaurants,getAdapterPosition());
                }
            });

        }
    }

    public class LoadMoreHolder extends ViewHolder {

        private ItemLoadMoreBinding mBinding;


        public LoadMoreHolder(ItemLoadMoreBinding itemRestaurantBinding) {
            super(itemRestaurantBinding.getRoot());
            mBinding = itemRestaurantBinding;

        }

        public void bindView() {

        }
    }
}
