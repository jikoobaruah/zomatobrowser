package jikoo.zomato.browser.view.common;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by jyotishman.baruah on 06/07/18.
 */


@GlideModule
public class ZomatoGlideModule extends AppGlideModule {
}