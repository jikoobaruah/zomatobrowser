package jikoo.zomato.browser.view.main;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ItemRestaurantBinding;
import jikoo.zomato.browser.model.dto.Restaurant;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class BottomSheetFragment extends BottomSheetDialogFragment {
    private ItemRestaurantBinding binding;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.item_restaurant,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setRestaurant((Restaurant) getArguments().getParcelable(ARGS.RESTAURANT));
        binding.executePendingBindings();
    }

    public static BottomSheetFragment getFragment(Context context,Restaurant restaurant) {
        Bundle args = new Bundle();
        args.putParcelable(ARGS.RESTAURANT, restaurant);
        return (BottomSheetFragment) Fragment.instantiate(context,BottomSheetFragment.class.getName(),args);
    }

    private static class ARGS {
        public static final String RESTAURANT = "restaurant";
    }
}