package jikoo.zomato.browser.view.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.jikoo.placepicker.LocationManager;
import com.jikoo.placepicker.utils.AddressUtils;
import com.jikoo.placepicker.viewModel.GeoCoderViewModel;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ActivityMainBinding;
import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.model.network.ApiException;
import jikoo.zomato.browser.view.search.SearchActivity;
import jikoo.zomato.browser.viewModel.LocationSearchViewModel;

public class MainActivity extends AppCompatActivity implements LocationManager.CallBacks, SearchListFragment.CallBack, SearchMapFragment.CallBack {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int LIST = 1;
    private static final int MAP = 2;

    private LocationManager mLocationManager;

    private GeoCoderViewModel mGeoCoderViewModel;
    private LocationSearchViewModel mLocationSearchViewModel;

    private ActivityMainBinding binding;


    private int mode = LIST;

    private Observer<ArrayList<Restaurant>> searchObserver = new Observer<ArrayList<Restaurant>>() {
        @Override
        public void onChanged(@Nullable ArrayList<Restaurant> restaurants) {
            if (restaurants == null || restaurants.size() == 0) {
                return;
            }
            hideProgress();

            if (mode == LIST) {
                gotoNewListView(restaurants);
            } else {
                gotoNewMapView(restaurants);
            }

        }
    };


    private Observer<Address> addressObserver = new Observer<Address>() {
        @Override
        public void onChanged(@Nullable Address address) {
            if (address != null) {
                mGeoCoderViewModel.getAddressLiveData().removeObserver(this);
                onAddressFetched(AddressUtils.getFormattedAddress(address), address.getLatitude(), address.getLongitude());
            }
        }
    };

    private Observer<ApiException> searchExceptionObserver = new Observer<ApiException>() {
        @Override
        public void onChanged(@Nullable ApiException e) {
            if (e != null){
                hideProgress();
                showError(e);
            }
        }
    };



    private boolean mIsInProgress;
    private CharSequence mAddress;
    private Double mLatitude = Double.MIN_VALUE;
    private double mLongitude = Double.MIN_VALUE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationManager = new LocationManager();
        mLocationManager.onCreate(this);

        mGeoCoderViewModel = ViewModelProviders.of(this).get(GeoCoderViewModel.class);
        mLocationSearchViewModel = ViewModelProviders.of(this).get(LocationSearchViewModel.class);

        mGeoCoderViewModel.getAddressLiveData().observe(this, addressObserver);
        mLocationSearchViewModel.getSearchLiveData().observe(this, searchObserver);
        mLocationSearchViewModel.getExceptionLiveData().observe(this, searchExceptionObserver);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(binding.layoutToolbar.toolbar);



        if (savedInstanceState == null) {
            mLocationManager.fetchLocation(this);
        } else {
            mAddress = savedInstanceState.getCharSequence(OUT_STATE.ADDRESS, null);
            mLatitude = savedInstanceState.getDouble(OUT_STATE.LATITUDE, Double.MIN_VALUE);
            mLongitude = savedInstanceState.getDouble(OUT_STATE.LONGITUDE, Double.MIN_VALUE);
            mode = savedInstanceState.getInt(OUT_STATE.MODE, LIST);
            mIsInProgress = savedInstanceState.getBoolean(OUT_STATE.IS_IN_PROGRESS, false);
            if (mIsInProgress) {
                showProgress();
            }
            if (mAddress != null){
                binding.layoutToolbar.tvSearchLocation.setText(mAddress);
            }
        }

        binding.layoutToolbar.viewSearch.tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(MainActivity.this,SearchActivity.class);
                searchIntent.putExtra(SearchActivity.ARGS.LATITUDE, mLatitude);
                searchIntent.putExtra(SearchActivity.ARGS.LONGITUDE, mLongitude);
                searchIntent.putExtra(SearchActivity.ARGS.ADDRESS, mAddress);
                startActivity(searchIntent);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(OUT_STATE.IS_IN_PROGRESS, mIsInProgress);
        outState.putDouble(OUT_STATE.LATITUDE, mLatitude);
        outState.putDouble(OUT_STATE.LONGITUDE, mLongitude);
        outState.putCharSequence(OUT_STATE.ADDRESS, mAddress);
        outState.putInt(OUT_STATE.MODE, mode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationManager.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!mLocationManager.onRequestPermissionsResult(this, requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mLocationManager.onActivityResult(this, requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "GPS location >> " + location);
        if (location == null) {
            mLocationManager.launchPlacePicker(this);
        } else {
            mLatitude = location.getLatitude();
            mLongitude = location.getLongitude();
            mGeoCoderViewModel.getAddressFromLatLong(location.getLatitude(), location.getLongitude());
        }
    }

    @Override
    public void onLocationFailed(Exception e) {
        Log.d(TAG, "Error trying to get last GPS location >> " + e.getMessage());
        mLocationManager.launchPlacePicker(this);
    }

    @Override
    public void onAddressFetched(CharSequence address, double lat, double lon) {
        Log.d(TAG, "onAddressFetched >> " + address + " location > " + lat + "," + lon);
        binding.layoutToolbar.tvSearchLocation.setText(address);
        binding.fragContainer.removeAllViews();

        mLatitude = lat;
        mLongitude =lon;
        mAddress = address;
        mIsInProgress = true;
        showProgress();
        mLocationSearchViewModel.searchByLatLon(address, lat, lon);


    }

    @Override
    public void gotoListView(ArrayList<Restaurant> restaurants) {
        mode = LIST;
        Fragment listFrag = getSupportFragmentManager().findFragmentByTag("listFrag");
        if (listFrag == null) {
            listFrag = SearchListFragment.getFragment(MainActivity.this, restaurants);
            getSupportFragmentManager().beginTransaction().add(R.id.frag_container, listFrag, "listFrag").commit();

        }

        getSupportFragmentManager().beginTransaction().show(listFrag).commit();
        Fragment mapFrag = getSupportFragmentManager().findFragmentByTag("mapFrag");
        if (mapFrag != null) {
            getSupportFragmentManager().beginTransaction().hide(mapFrag).commit();
        }
    }

    @Override
    public void gotoMapView(ArrayList<Restaurant> restaurants) {
        mode = MAP;

        Fragment mapFrag = getSupportFragmentManager().findFragmentByTag("mapFrag");
        if (mapFrag == null) {
            mapFrag = SearchMapFragment.getFragment(MainActivity.this, restaurants,mLatitude,mLongitude);
            getSupportFragmentManager().beginTransaction().add(R.id.frag_container, mapFrag, "mapFrag").commit();
        }

        getSupportFragmentManager().beginTransaction().show(mapFrag).commit();
        Fragment listFrag = getSupportFragmentManager().findFragmentByTag("listFrag");
        if (listFrag != null) {
            getSupportFragmentManager().beginTransaction().hide(listFrag).commit();
        }
    }

    @Override
    public void onMapLocationChanged(LatLng target) {
        mLatitude = target.latitude;
        mLongitude = target.longitude;
        mGeoCoderViewModel.getAddressFromLatLong(mLatitude,mLongitude);
    }

    public void changeLocation(View view) {
        mLocationManager.launchPlacePicker(this);
    }

    private void showError(ApiException e) {
        binding.layoutProgress.rlProgressContainer.setVisibility(View.VISIBLE);
        binding.layoutProgress.progress.setVisibility(View.GONE);
        binding.layoutProgress.ivError.setVisibility(View.VISIBLE);
        binding.layoutProgress.btnRetry.setVisibility(View.VISIBLE);
        binding.layoutProgress.btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLocationSearchViewModel.searchByLatLon(mAddress, mLatitude, mLongitude);
            }
        });
        binding.layoutProgress.tvStatus.setText(e.getError().getStatus());
//        binding.layoutProgress.tvMessage.setText(e.getError().getMessage());
//        binding.layoutProgress.tvMessage.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        binding.layoutProgress.rlProgressContainer.setVisibility(View.VISIBLE);
        binding.layoutProgress.progress.setVisibility(View.VISIBLE);
        binding.layoutProgress.ivError.setVisibility(View.GONE);
        binding.layoutProgress.btnRetry.setVisibility(View.GONE);
        binding.layoutProgress.tvStatus.setText(getResources().getText(R.string.search_progress));
//        binding.layoutProgress.tvMessage.setVisibility(View.GONE);
    }

    private void hideProgress() {
        mIsInProgress = false;
        binding.layoutProgress.rlProgressContainer.setVisibility(View.GONE);
    }

    private void gotoNewMapView(ArrayList<Restaurant> restaurants) {
        mode = MAP;
        Fragment mapFrag = SearchMapFragment.getFragment(MainActivity.this, restaurants,mLatitude,mLongitude);
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, mapFrag, "mapFrag").commit();

    }

    private void gotoNewListView(ArrayList<Restaurant> restaurants) {
        mode = LIST;
        Fragment listFrag = SearchListFragment.getFragment(MainActivity.this, restaurants);
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, listFrag, "listFrag").commit();

    }

    private interface OUT_STATE {
        String IS_IN_PROGRESS = "iip";
        String LATITUDE = "lat";
        String LONGITUDE = "lon";
        String ADDRESS = "add";
        String MODE = "mode";
    }
}
