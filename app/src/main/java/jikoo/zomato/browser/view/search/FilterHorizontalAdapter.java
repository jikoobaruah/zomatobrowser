package jikoo.zomato.browser.view.search;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import jikoo.zomato.browser.R;
import jikoo.zomato.browser.databinding.ItemCounterBinding;
import jikoo.zomato.browser.databinding.ItemFilterBinding;

/**
 * Created by jyotishman.baruah on 08/07/18.
 */

public class FilterHorizontalAdapter<T> extends RecyclerView.Adapter<FilterHorizontalAdapter.ViewHolder> {

    private static final int VIEW_TYPE_TEXT = 1;
    private static final int VIEW_TYPE_COUNTER = 2;
    private final int DISPLAY_COUNT = 4;

    private ArrayList<T> filters;

    public FilterHorizontalAdapter() {
        filters = new ArrayList<>();
    }

    @NonNull
    @Override
    public FilterHorizontalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_COUNTER) {
            ItemCounterBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_counter, parent, false);
            return new CounterViewHolder(itemBinding);
        } else {
            ItemFilterBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_filter, parent, false);
            return new FilterViewHolder(itemBinding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull FilterHorizontalAdapter.ViewHolder  holder, int position) {
        holder.bindView();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == DISPLAY_COUNT - 1 && filters.size() > DISPLAY_COUNT) {
            return VIEW_TYPE_COUNTER;
        }
        return VIEW_TYPE_TEXT;
    }

    @Override
    public int getItemCount() {
        if (filters.size() > DISPLAY_COUNT) {
            return DISPLAY_COUNT;
        }
        return filters.size();
    }

    public void addAll(ArrayList<T> filters) {
        if (filters == null || filters.size() == 0)
            return;
        int start = this.filters.size();
        this.filters.addAll(filters);
        notifyItemRangeChanged(start,filters.size());

    }

    public  abstract class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bindView();
    }

    public class CounterViewHolder extends ViewHolder {
        private ItemCounterBinding mBinding;
        public CounterViewHolder(ItemCounterBinding itemBinding) {
            super(itemBinding.getRoot());
            mBinding = itemBinding;
        }

        @Override
        public void bindView() {
            int left = filters.size() - DISPLAY_COUNT + 1;
            mBinding.tvMore.setText(left + "more");
        }
    }

    public class FilterViewHolder extends ViewHolder {
        private ItemFilterBinding mBinding;

        public FilterViewHolder(ItemFilterBinding itemBinding) {
            super(itemBinding.getRoot());
            mBinding = itemBinding;
        }

        @Override
        public void bindView() {
            mBinding.tvFilter.setText(filters.get(getAdapterPosition()).toString());
        }
    }
}
