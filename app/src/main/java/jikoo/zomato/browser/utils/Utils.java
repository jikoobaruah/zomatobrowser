package jikoo.zomato.browser.utils;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import jikoo.zomato.browser.model.dto.Restaurant;
import jikoo.zomato.browser.view.details.DetailsActivity;

/**
 * Created by jyotishman.baruah on 11/07/18.
 */

public class Utils {
    public static void launchDetailsView(Context context, ArrayList<Restaurant> restaurants, int adapterPosition) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(DetailsActivity.ARGS.LIST,restaurants);
        intent.putExtra(DetailsActivity.ARGS.POSITION,adapterPosition);
        context.startActivity(intent);
    }
}
