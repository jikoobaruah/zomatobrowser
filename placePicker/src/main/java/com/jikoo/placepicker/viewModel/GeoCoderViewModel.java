package com.jikoo.placepicker.viewModel;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.location.Address;
import android.support.annotation.NonNull;

import com.jikoo.placepicker.data.repo.GeoCoderRepository;
import com.jikoo.placepicker.utils.NoAddressFoundException;

/**
 * Created by jyotishman.baruah on 04/07/18.
 */

public class GeoCoderViewModel extends AndroidViewModel {

    private GeoCoderRepository mGeoCoderRepository;


    public GeoCoderViewModel(@NonNull Application application) {
        super(application);
        mGeoCoderRepository = GeoCoderRepository.getInstance();

    }

    public MutableLiveData<Address> getAddressLiveData() {
        return mGeoCoderRepository.getAddressLiveData();
    }

    public void getAddressFromLatLong(final double lat, final double lon) {
        mGeoCoderRepository.getAddressFromLatLong(this.getApplication().getApplicationContext(), lat, lon);
    }

    public MutableLiveData<NoAddressFoundException> getAddressErrorLiveData() {
        return mGeoCoderRepository.getAddressErrorLiveData();
    }
}

