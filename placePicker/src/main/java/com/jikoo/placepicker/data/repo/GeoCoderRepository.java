package com.jikoo.placepicker.data.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.jikoo.placepicker.utils.NoAddressFoundException;
import com.jikoo.placepicker.utils.SingleLiveEvent;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class GeoCoderRepository {

    private static final Executor sExecutor = Executors.newSingleThreadExecutor();
    private static GeoCoderRepository sRepository;
    private SingleLiveEvent<Address> mAddressLiveData;
    private SingleLiveEvent<NoAddressFoundException> mAddressErrorLiveData;


    private GeoCoderRepository() {
        mAddressLiveData = new SingleLiveEvent<>();
        mAddressErrorLiveData = new SingleLiveEvent<>();
    }

    public static GeoCoderRepository getInstance() {
        if (sRepository == null) {
            sRepository = new GeoCoderRepository();
        }
        return sRepository;
    }

    public void getAddressFromLatLong(Context context, final double lat, final double lon) {
        final Geocoder geocoder = new Geocoder(context.getApplicationContext());
        sExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Address> address = geocoder.getFromLocation(lat, lon, 1);
                    if (address != null && address.size() > 0) {
                        mAddressLiveData.postValue(address.get(0));
                    }else{
                        mAddressErrorLiveData.postValue(new NoAddressFoundException(lat,lon));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MutableLiveData<Address> getAddressLiveData() {
        return mAddressLiveData;
    }

    public MutableLiveData<NoAddressFoundException> getAddressErrorLiveData() {
        return mAddressErrorLiveData;
    }


}
