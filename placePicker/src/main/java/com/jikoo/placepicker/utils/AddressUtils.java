package com.jikoo.placepicker.utils;

import android.location.Address;

/**
 * Created by jyotishman.baruah on 05/07/18.
 */

public class AddressUtils {
    public static CharSequence getFormattedAddress(Address address) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
            sb.append(address.getAddressLine(i));
            if (i < address.getMaxAddressLineIndex() - 1) {
                sb.append(",");
            }
        }
        return sb;
    }
}
