package com.jikoo.placepicker.view;

import android.animation.ObjectAnimator;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.jikoo.placepicker.R;
import com.jikoo.placepicker.utils.NoAddressFoundException;
import com.jikoo.placepicker.viewModel.GeoCoderViewModel;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, PlaceSelectionListener, GoogleMap.OnCameraMoveStartedListener {

    public static final String EXTRA_ADDRESS = "mAddress";
    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;

    private SupportPlaceAutocompleteFragment mPlaceAutocompleteFragment;

    private Toolbar mToolbar;
    private Address mAddress;

    private GeoCoderViewModel mGeoCoderViewModel;
    private ImageView mImageView;

    private Observer<Address> mAddressObserver = new Observer<Address>() {
        @Override
        public void onChanged(@Nullable Address address) {
            Log.d(TAG, "onChanged address >>" + address);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            if (address != null) {
                mAddress = address;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i));
                    if (i < address.getMaxAddressLineIndex() - 1) {
                        sb.append(",");
                    }
                }
                mPlaceAutocompleteFragment.setText(sb);
            }
        }
    };

    private Observer<NoAddressFoundException> mAddressErrorObserver = new Observer<NoAddressFoundException>() {
        @Override
        public void onChanged(@Nullable NoAddressFoundException e) {
            Log.d(TAG, "NoAddressFoundException >>" );
            mMap.getUiSettings().setScrollGesturesEnabled(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mToolbar = findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImageView = findViewById(R.id.iv_point);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mPlaceAutocompleteFragment = (SupportPlaceAutocompleteFragment) getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        mPlaceAutocompleteFragment.setOnPlaceSelectedListener(this);

        mGeoCoderViewModel = ViewModelProviders.of(MapsActivity.this).get(GeoCoderViewModel.class);
        mGeoCoderViewModel.getAddressLiveData().observe(this, mAddressObserver);
        mGeoCoderViewModel.getAddressErrorLiveData().observe(this, mAddressErrorObserver);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.place_picker, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_done) {
            if (mAddress == null) {
                return true;
            }
            Intent data = new Intent();
            data.putExtra(EXTRA_ADDRESS, mAddress);
            setResult(RESULT_OK, data);
            finish();
            return true;
        } else if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);

    }

    @Override
    public void onCameraMoveStarted(int i) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(mImageView, "translationY", -100);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
        mAddress = null;
    }


    @Override
    public void onCameraIdle() {
        Log.d(TAG, "center location >>" + mMap.getCameraPosition().target);
        ObjectAnimator anim = ObjectAnimator.ofFloat(mImageView, "translationY", 0);
        anim.setInterpolator(new BounceInterpolator());
        anim.start();
        getPlaceFromLatLong(mMap.getCameraPosition().target);
    }


    @Override
    public void onPlaceSelected(Place place) {
        Log.d(TAG, "onPlaceSelected >>" + place.getLatLng());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(),12));
    }

    @Override
    public void onError(Status status) {

    }


    private void getPlaceFromLatLong(final LatLng target) {
        Log.d(TAG, "getPlaceFromLatLong >>" + target);
        mPlaceAutocompleteFragment.setText(String.format("%f,%f",target.latitude,target.longitude));
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mGeoCoderViewModel.getAddressFromLatLong(target.latitude, target.longitude);
    }




}
