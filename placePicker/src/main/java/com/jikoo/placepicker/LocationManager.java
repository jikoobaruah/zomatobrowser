package com.jikoo.placepicker;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jikoo.placepicker.view.MapsActivity;

import java.lang.ref.WeakReference;


/**
 * Created by jyotishman.baruah on 04/07/18.
 */

public class LocationManager {

    private static final String TAG = LocationManager.class.getSimpleName();

    private static final int PERMISSION_REQUEST_CODE = 989;

    private static final int PLACE_PICKER_REQUEST = 345;
    private static final int CUSTOM_PLACE_PICKER_REQUEST = 346;

    private static boolean isPermissionRequested;

    private WeakReference<CallBacks> mCallBack;
    private Dialog mAlertDialog;



    public void onCreate(CallBacks callBack) {

        mCallBack = new WeakReference<>(callBack);
    }


    public void fetchLocation(Activity activity){

        if (checkPermission(activity)) {
            getLastLocation(activity);
        } else {
            requestPermission(activity);
        }

    }

    public void onDestroy() {
        if (mCallBack != null) {
            mCallBack.clear();
        }

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
        mAlertDialog = null;

    }


    public boolean onRequestPermissionsResult(Activity activity, int requestCode, String permissions[], int[] grantResults) {
        if (requestCode != PERMISSION_REQUEST_CODE) {
            return false;
        }

        isPermissionRequested = false;

        if (grantResults.length == 0) {
            return true;
        }

        boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        if (locationAccepted) {
            getLastLocation(activity);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity.shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
            showPermissionInfoDialog(activity);
        } else if (mCallBack.get() != null) {
            mCallBack.get().onLocationFailed(new RuntimeException("User denied permission"));
        }
        return true;
    }


    public boolean onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode != PLACE_PICKER_REQUEST && requestCode != CUSTOM_PLACE_PICKER_REQUEST) {
            return false;
        }

        if (requestCode == CUSTOM_PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            Address address = data.getParcelableExtra(MapsActivity.EXTRA_ADDRESS);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                sb.append(address.getAddressLine(i));
                if (i < address.getMaxAddressLineIndex()) {
                    sb.append(",");
                }
            }
            if (mCallBack.get() != null) {
                mCallBack.get().onAddressFetched(sb.toString(), address.getLatitude(), address.getLongitude());
            }
        } else if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            Place place = PlacePicker.getPlace(activity, data);
            if (mCallBack.get() != null) {
                mCallBack.get().onAddressFetched(place.getAddress(), place.getLatLng().latitude, place.getLatLng().longitude);
            }
        }

        return true;
    }


    public boolean checkPermission(Activity activity) {
        return ContextCompat.checkSelfPermission(activity.getApplicationContext(), ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    public void launchPlacePicker(Activity activity) {
        if (checkPermission(activity)) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

            try {
                activity.startActivityForResult(builder.build(activity), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
                launchCustomPlacePicker(activity);
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
                launchCustomPlacePicker(activity);
            }
        } else {
            launchCustomPlacePicker(activity);
        }
    }


    private void requestPermission(Activity activity) {
        if (isPermissionRequested) {
            return;
        }
        showPermissionInfoDialog(activity);

    }

    private void showPermissionInfoDialog(final Activity activity) {
        mAlertDialog = new AlertDialog.Builder(activity)
                .setMessage(activity.getResources().getString(R.string.location_permission_explanation))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isPermissionRequested = true;
                        ActivityCompat.requestPermissions(activity, new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mCallBack.get() != null) {
                            mCallBack.get().onLocationFailed(new RuntimeException("User denied permission"));
                        }
                    }
                })
                .setCancelable(false)
                .create();
        mAlertDialog.show();
    }


    private void getLastLocation(Activity activity) {
        if (!checkPermission(activity)) {
            return;
        }

        FusedLocationProviderClient locationClient = LocationServices.getFusedLocationProviderClient(activity);


        locationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Log.d(TAG, "GPS location >> " + location);
                        // GPS location can be null if GPS is switched off
                        if (location != null && mCallBack.get() != null) {
                            mCallBack.get().onLocationChanged(location);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Error trying to get last GPS location");
                        e.printStackTrace();
                        if (mCallBack.get() != null) {
                            mCallBack.get().onLocationFailed(e);
                        }
                    }
                });
    }


    private void launchCustomPlacePicker(Activity activity) {
        Intent intent = new Intent(activity, MapsActivity.class);
        activity.startActivityForResult(intent, CUSTOM_PLACE_PICKER_REQUEST);
    }


    public interface CallBacks {
        void onLocationChanged(Location location);

        void onLocationFailed(Exception e);

        void onAddressFetched(CharSequence address, double lat, double lon);

    }
}
